Required python packages:
----------------------
Run "python setup.py install" in terminal for each package.

IOs:
git://github.com/adafruit/adafruit-beaglebone-io-python.git

SPI (if not using adafruit):
https://github.com/doceme/py-spidev.git

Network:
https://github.com/raphdg/netifaces.git


MQTT Broker:
--------------------
Free to use on Eclipse server
iot.eclipse.org:1883

To set up your own local server, see this link (for Linux):
http://www.switchdoc.com/2016/02/tutorial-installing-and-testing-mosquitto-mqtt-on-raspberry-pi/
Use server address <IP>:1883