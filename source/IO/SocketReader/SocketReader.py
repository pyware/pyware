#!/usr/bin/python

from Threading.ThreadingBase    import ThreadingBase
import time, socket
            
class SocketReader(ThreadingBase):
    def __init__(self, buffer, server):
        ThreadingBase.__init__(self, -1, "SocketReaderThread")
        
        self.buffer     = buffer
        self.server     = server
        self.connected  = False
        
    def run(self):
        print "Starting " + self.name
        self.connected = True
        
        while not self._exitFlag:
            try:
                self.work()
                #self.wait()
                # Use socket timeout instead of wait()
            #except socket.timeout, e:
            #    print "Timeout in %s: %s" %(self.name, e)
            except socket.error, e:
                #print "Error in %s: %s" %(self.name, e)
                self.server.closeConnection()
                break
                
        self.connected = False         
        print "Exiting " + self.name
        
    def work(self):
        self.__writeBuffer(self.__readSocket())
        
    def checkConnection(self):
        return self.connected
            
    def __writeBuffer(self, data):
        # Writes data to output buffer
        if data:
            self.buffer.appendChunk(data)
            
    def __readSocket(self):
        # Receive data from client
        try:
            data = self.server.receiveData()
            if data:
                #print "Received %d elements from client." %len(data)
                pass
            return data
        except socket.timeout, e:
            #print "Error: %s" %e
            return None
            
