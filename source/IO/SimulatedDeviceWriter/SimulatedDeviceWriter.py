#!/usr/bin/python

from Threading.ThreadingBase    import ThreadingBase
import time

class SimulatedDeviceWriter(ThreadingBase):
    def __init__(self, bufferOut, numChannels=1):
        ThreadingBase.__init__(self, -1, "SimulatedDeviceWriterThread")

        self.buffer     = bufferOut
        self.numCh      = numChannels
        
        self.setLoopRate(1)
        
    def work(self):
        data = self.__readBuffer()
        if data:
            if len(data) == self.numCh:
                self.__writeOutput(data)
            else:
                raise Exception("IO missmatch: Data size read from buffer do not match number of channels.")

    def __writeOutput(self, data):
        print "Output data: %s" %data
        
    def __readBuffer(self):
        # Reads output buffer if larger than num of channels.
        return self.buffer.popChunk()
        