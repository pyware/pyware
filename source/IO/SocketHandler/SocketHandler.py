#!/usr/bin/python

from Threading.ThreadingBase            import ThreadingBase
from Socket.SocketServer                import SocketServer
from IO.SocketReader.SocketReader       import SocketReader
from IO.SocketWriter.SocketWriter       import SocketWriter
import time, socket
            
class SocketHandler(ThreadingBase):
    def __init__(self, bufferIn, bufferOut):
        ThreadingBase.__init__(self, -1, "SocketHandlerThread")
        
        self.bufferIn   = bufferIn
        self.bufferOut  = bufferOut
        self.version    = 0
        
        self.server = SocketServer(port=10000)
        
        # Set default loop rate
        self.setLoopRate(1)
        
    def setLoopRate(self, loopRate):
        lr = float(loopRate)
        super(SocketHandler, self).setLoopRate(lr)
        #self.server.setTimeout(lr/2)
        self.loopRate = lr
        
    def wait(self):
        time.sleep(1)
    
    def run(self):
        print "Starting " + self.name
        
        while not self._exitFlag:
            try:
                # Wait for client to connect
                self.server.acceptConnection()
                
                # Init connection
                initData = self.server.initConnection([float(self.version)])
                print "Init data: %s" %initData
                
                # Create and start socket reader and writer threads
                # Thread can only be started once
                self.sw = SocketWriter(self.bufferIn, self.server)
                self.sr = SocketReader(self.bufferOut, self.server)
                
                self.sw.setLoopRate(self.loopRate)
                self.sr.setLoopRate(self.loopRate) 
                
                self.sw.start()
                self.sr.start()
                
                # Wait for threads to start before reading flags
                self.wait()
                
                # Check connection for socket reader and writer
                while not self._exitFlag:
                    if self.sw.checkConnection() and self.sr.checkConnection():
                        #print "Connected..."
                        self.wait()
                    else:
                        break
                 
                # Stop socket reader and writer threads
                self.sw.join()
                self.sr.join()

            except socket.timeout, e:
                #print "Error: %s" %e
                print "Listener timed out."
            
        self.server.close()       
        print "Exiting " + self.name
        