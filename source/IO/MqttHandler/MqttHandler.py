#!/usr/bin/python

from Threading.ThreadingBase            import ThreadingBase
from Utils.Utils                        import Utils

from struct import unpack, pack
import paho.mqtt.client as mqtt
import time

class MqttSettings:
    # Set default host address
    url = "iot.eclipse.org"
    port = 1883
    # Set default topics
    pubTopic = "dvelab/test01"
    subTopic = "dvelab/#"

class MqttHandler(ThreadingBase):
    def __init__(self, bufferIn, bufferOut, mqttSettings=None): 
        ThreadingBase.__init__(self, -1, "MqttHandlerThread")
        
        self.client = mqtt.Client()
        # Register callbacks
        self.client.on_connect      = self.__on_connect
        self.client.on_disconnect   = self.__on_disconnect
        self.client.on_message      = self.__on_message
        
        self.bufferIn = bufferIn
        self.bufferOut = bufferOut
        
        # Check if mqttSettings object is valid, otherwise use default settings
        if mqttSettings:
            self.mqttSettings = mqttSettings
        else:
            self.mqttSettings = MqttSettings()
            
        self.utils = Utils()

        # Set default loop rate
        self.setLoopRate(1)
    
    def work(self): # Override 
        # Read data from buffer, encode message and publish message
        data = self.__readBuffer()
        msg = self.__encodeMessage(data)
        self.__publishMessage(msg)
    
    def start(self): # Override
        # Connect and start the MQTT loop
        self.client.connect(self.mqttSettings.url, self.mqttSettings.port, 60)
        self.client.loop_start()
        super(MqttHandler, self).start()
        
    def join(self): # Override
        # Stop MQTT loop and disconnect
        self.client.loop_stop(force=False)
        self.client.disconnect()
        super(MqttHandler, self).join()
        
    # The callback for when the client receives a CONNACK response from the server.
    def __on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))
        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        client.subscribe(self.mqttSettings.subTopic, qos=2)
        
    # The callback for when the client sends a disconnect message.
    def __on_disconnect(self, client, userdata, rc):
        print("Disconnected with result code "+str(rc))
        client.unsubscribe(self.mqttSettings.subTopic)

    # The callback for when a PUBLISH message is received from the server.
    def __on_message(self, client, userdata, msg):
        #print(msg.topic+" "+str(msg.payload))
        data = self.__decodeMessage(msg.payload)
        #print data
        self.__writeBuffer(data)

    def __decodeMessage(self, msg):
        crc_ok = False
        if msg:
            crc = msg[:4] # First 4 bytes are encoded crc
            payload = msg[4:] # Rest is encoded payload
            crc_local = self.utils.crc32(payload)
            crc_remote = unpack('>I', crc)[0]
            crc_ok = (crc_local == crc_remote)
            if crc_ok:
                # Unpacks received byte string to double data list
                return self.utils.unpackList('>d', payload)
            else:
                print crc_local
                print crc_remote
                raise Exception("CRC missmatch: Received CRC do not match calculated CRC.")
        else:
            return None
    
    def __encodeMessage(self, data):
        # Packs double data list to a byte string
        if data:
            byteStr = self.utils.packList('>d', data)
            return bytearray(pack('>I', self.utils.crc32(byteStr)) + byteStr)
        else:
            return None
        
    def __publishMessage(self, msg):
        # Add and pack CRC before encoded message
        if msg:
            self.client.publish(self.mqttSettings.pubTopic, msg, qos=2)
            
    def __readBuffer(self):
        # Reads buffer until empty or num of remaining elements less than num of channels
        return self.bufferIn.popChunkAll()
        
    def __writeBuffer(self, data):
        # Writes data to output buffer
        if data:
            self.bufferOut.appendChunk(data)
