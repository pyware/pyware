#!/usr/bin/python

from Threading.ThreadingBase    import ThreadingBase
import time, socket
            
class SocketWriter(ThreadingBase):
    def __init__(self, buffer, server):
        ThreadingBase.__init__(self, -1, "SocketWriterThread")
        
        self.buffer     = buffer
        self.server     = server
        self.connected  = False
        
    def run(self):
        print "Starting " + self.name
        self.connected = True
        
        while not self._exitFlag:
            try:
                self.work()
                self.wait()
            #except socket.timeout, e:
            #    print "Timeout in %s: %s" %(self.name, e)
            except socket.error, e:
                #print "Error in %s: %s" %(self.name, e)
                self.server.closeConnection()
                break

        self.connected = False         
        print "Exiting " + self.name
        
    def work(self):
        self.__writeSocket(self.__readBuffer())
        
    def checkConnection(self):
        return self.connected

    def __readBuffer(self):
        # Reads buffer until empty or num of remaining elements less than num of channels.
        return self.buffer.popChunkAll()
        
    def __writeSocket(self, data):
        # Send data to client
        if data:
            self.server.sendData(data)
            #print "Sent %d elements to client." %len(data)
