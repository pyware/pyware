#!/usr/bin/python

from Threading.ThreadingBase    import ThreadingBase
import time, random

class SimulatedDeviceReader(ThreadingBase):
    def __init__(self, bufferIn, numChannels=1):
        ThreadingBase.__init__(self, -1, "SimulatedDeviceReaderThread")

        self.buffer     = bufferIn
        self.numCh      = numChannels
        self.data       = []
        
        self.setLoopRate(1)
        
    def work(self):
        self.__readSensor()
        self.__writeBuffer()
        
        # Debug
        #print "Buffer size = %d" %len(self.buffer)
        #print "Size=%d, Buffer=%s" %(len(self.buffer), self.buffer)
        
    def __readSensor(self):
        self.data = []
        for i in range(self.numCh):
            self.data.append(random.randrange(-100,100))
        #print self.data
        
    def __writeBuffer(self):
        if self.data:
            self.buffer.append(time.time())
            for i in range(len(self.data)):
                self.buffer.append(self.data[i])
