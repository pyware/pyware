#!/usr/bin/python

from Utils.Metronome.Metronome import Metronome

from threading      import Thread
import time

class ThreadingBase(Thread):
    def __init__(self, id, name=""):
        Thread.__init__(self)
        
        self._exitFlag  = False
        self.id         = id
        
        # Use separate metronome thread instead of sleep 
        # to increase loop rate accuracy
        self.metronome = Metronome()
        self.metronome.setLoopRate(1)
        self.metronome.start()
        
        if len(name):
            self.name = name
        else:
            if id < 0:
                self.name = "Thread"
            else:
                self.name = "Thread%d" %self.id
   
    def setLoopRate(self, loopRate):
        self.metronome.setLoopRate(loopRate)
    
    def wait(self):
        self.metronome.wait()
 
    def run(self):
        print "Starting " + self.name
        while not self._exitFlag:
            self.work()
            self.wait()
        print "Exiting " + self.name
        
    def work(self):
        # Should be overridden
        raise NotImplementedError
        
    def join(self):
        self._exitFlag = True
        super(ThreadingBase, self).join()
        self.metronome.join()
        
    def _getClassName(self):
        return self.__name__.split(".")[-1]
        