#!/usr/bin/python

from threading      import Thread
import time

class ThreadingBase(Thread):
    def __init__(self, id, name=""):
        Thread.__init__(self)
        
        self._exitFlag  = False
        self.id         = id
        self.loopRate	= 1.0
        self.nextCall	= time.time()
        
        if len(name):
            self.name = name
        else:
            if id < 0:
                self.name = "Thread"
            else:
                self.name = "Thread%d" %self.id
   
    def setLoopRate(self, loopRate):
        self.loopRate = float(loopRate)
    
    def wait(self):
        self.nextCall = self.nextCall + self.loopRate
        wait = self.nextCall - time.time()
        if wait > 0:
            #print "Loop sleep time = %f s" %wait
            time.sleep(wait)
        else:
            print "Warning: Loop rate exceeded in %s (diff = %f s)." %(self.name, wait)
 
    def run(self):
        print "Starting " + self.name
        self.nextCall = time.time()
        while not self._exitFlag:
            self.work()
            self.wait()
        print "Exiting " + self.name
        
    def work(self):
        # Should be overridden
        raise NotImplementedError
        
    def join(self):
        self._exitFlag = True
        super(ThreadingBase, self).join()
        
    def _getClassName(self):
        return self.__name__.split(".")[-1]
        