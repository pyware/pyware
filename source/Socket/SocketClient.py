#!/usr/bin/python

from Utils.Utils        import Utils
from struct             import unpack, pack

import time, socket

class SocketClient(object):
    def __init__(self, address=None, port=10000): 

        self._address           = address
        self._port              = port
        self._connected         = False

        self.ignoreConnError    = False
        self.socket             = None
        self.__socketTimeout    = None

        self.utils              = Utils()
        
        print 'Socket client started'

    def _createSocket(self):
        if self.socket:
            pass
        else:
            #self.socket = socket.create_connection((self._address,self._port), timeout=10)
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # Nagle disabled
            self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True) # added line

    def _closeSocket(self):
        if self.socket:
            self.socket.close()
        self.socket = None

    def __sendMessageCrc(self, msg):
        if msg:
            m = pack('>I', len(msg)) + msg + pack('>I', self.utils.crc32(msg))
            self.socket.sendall(m)

    def __receiveMessageCrc(self):
        msgSize = self.socket.recv(4)
        crc_ok = False
        if msgSize:
            msgSize = unpack('>I', str(msgSize))[0]
            msg = self.socket.recv(msgSize)
            crc_local = self.utils.crc32(msg)
            crc_remote = unpack('>I', str(self.socket.recv(4)))[0]
            crc_ok = (crc_local == crc_remote)
            if crc_ok:
                return msg
            else:
                raise Exception("Socket CRC missmatch")
        else:
            return None

    def _setSocketTimeout(self):
        timeout = self.__socketTimeout
        #if self.socket:
        if timeout < 0:
            # Disable timeout
            self.socket.setblocking(1)
            #self.socket.settimeout(None)
        else:
            self.socket.setblocking(0)
            self.socket.settimeout(timeout)

    def _handleError(self, error):
        if self.ignoreConnError:
            try:
                raise error
            except socket.timeout, e:
                print "Warning: %s"%e
                self.closeConnection()
            except socket.error, e:
                print "Warning: %s"%e
                self.closeConnection()
                time.sleep(1)
        else:
            raise error

    def setSocketTimeout(self, timeout):
        self.__socketTimeout = float(timeout)

    def openConnection(self):
        server_address = (self._address, self._port)
        self._createSocket()
        #print self.socket
        self._setSocketTimeout()
        self.socket.connect(server_address)
        self._connected = True
        print 'Connected to server', server_address

    def closeConnection(self):
        self._closeSocket()
        self._connected = False
        print 'Disconnected'

    def close(self):
        self.closeConnection()
        self._closeSocket()

    def initData(self, initData):
        # Receive server init data and send client init data
        data = self.receiveData()
        self.sendData(initData)
        return data

    def sendData(self, data):
        # Packs double data list to a byte string before send
        if data:
            byteStr = self.utils.packList('>d', data)
            self.sendMessage(byteStr)

    def receiveData(self):
        # Unpacks received byte string to double data list
        byteStr = self.receiveMessage()
        if byteStr:
            return self.utils.unpackList('>d', byteStr)
        else:
            return None

    def receiveMessage(self):
        msg = None
        try:
            if self._connected:
                #print "Try receive"
                msg = self.__receiveMessageCrc()
                if not msg:
                    #print "No msg"
                    self.closeConnection()
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)
        return msg

    def sendMessage(self, msg):
        try:
            if self._connected:
                #print "Try send"
                self.__sendMessageCrc(msg)
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)

    def transferMessage(self, msgOut):
        msgIn = None
        try:
            if self._connected:
                #print "Try transfer"
                self.__sendMessageCrc(msgOut)
                msgIn = self.__receiveMessageCrc()
                if not msgIn:
                    #print "No msg"
                    self.closeConnection()
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)
        return msgIn
