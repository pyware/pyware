#!/usr/bin/python

from Utils.Utils        import Utils
from struct             import unpack, pack

import socket, select, netifaces
import os, time

class SocketServer(object):
    def __init__(self, port=10000):     
        self.utils = Utils()
        
        # Find server IP address
        device = 'eth0' # Beaglebone: LAN = 'eth0', Mac: LAN = 'en0', WLAN = 'en1'
        server_ip = netifaces.ifaddresses(device)[netifaces.AF_INET][0]['addr'] # Beaglebone: LAN = 'eth0' 
        
        # Set default timeout for socket
        #socket.setdefaulttimeout(10.0)
        #print socket.getdefaulttimeout()
        
        # Create a TCP/IP socket
        self.__sock     = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__conn     = None
        
        # Set non-blocking and timeout for socket, accept()
        self.__sock.setblocking(0)
        self.__sock.settimeout(10.0)
        
        # Connection timeout, send and recv
        self.__timeout = 1.0
        
        # Bind the socket to the port
        server_address = (server_ip, port)
        self.__sock.bind(server_address)
        
        # Listen for incoming connections, 1 client
        self.__sock.listen(1)
        
        print 'TCP server started', server_address
    
    def close(self):
        self.__sock.close()
        
    def initConnection(self, initData):
        # Send server init data and receive client init data
        self.sendData(initData)
        return self.receiveData()
        
    def setTimeout(self, timeout):
        self.__timeout = float(timeout)
        
    def acceptConnection(self):
        # Wait for a client to connect, set timeout for connection
        print 'Waiting for incoming client connection...'
        self.__conn, client_address = self.__sock.accept()
        self.__conn.settimeout(self.__timeout)
        print 'Client connected', client_address
        
    def closeConnection(self):
        # Close the client connection
        self.__conn.close()
        print 'Client disconnected.'
        
    def sendData(self, data):
        # Packs double data list to a byte string before send
        if data:
            #t1 = time.time()
            byteStr = self.utils.packList('>d', data)
            #t2 = time.time()
            self.sendMessage(byteStr)
            #t3 = time.time()
            #print "Pack time = %f s" %(t2-t1)
            #print "Send time = %f s" %(t3-t2)
        
    def receiveData(self):
        # Unpacks received byte string to double data list
        byteStr = self.receiveMessage()
        if byteStr:
            return self.utils.unpackList('>d', byteStr)
        else:
            return None
        
    def sendMessage(self, msg):
        if msg:
            m = pack('>I', len(msg)) + msg + pack('>I', self.utils.crc32(msg))
            self.__conn.sendall(m)
            
    def receiveMessage(self):
        msgSize = self.__conn.recv(4)
        crc_ok = False
        if msgSize:
            msgSize = unpack('>I', str(msgSize))[0]
            msg = self.__conn.recv(msgSize)
            crc_local = self.utils.crc32(msg)
            crc_remote = unpack('>I', str(self.__conn.recv(4)))[0]
            crc_ok = (crc_local == crc_remote)
            if crc_ok:
                return msg
            else:
                print crc_local
                print crc_remote
                raise Exception("CRC missmatch: Received CRC do not match calculated CRC.")
        else:
            return None
