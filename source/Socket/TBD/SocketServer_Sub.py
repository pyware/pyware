#!/usr/bin/python

from Socket.SocketBase  import SocketBase

import netifaces

class SocketServer(SocketBase):
    def __init__(self, address=None, port=10000): 
        SocketBase.__init__(self, address, port)
        
        self.__remoteAddress    = None
        self.__remotePort       = None
        self.__connection       = None
        self.__connTimeout      = None
        
        # Find server IP address
        if not self._address:
            self._address = self.__findIpAddress()
            
        self._createSocket()
        
        # Bind the socket listener to the address and port
        server_address = (self._address, self._port)
        self.socket.bind(server_address)
        
        # Listen for incoming connections, 1 client allowed
        self.socket.listen(1)
        
        # Set default socket connection timeout (send and recv)
        #self.setConnTimeout(-1) # Timeout disabled 

        print 'Socket server started', server_address
    
    def __findIpAddress(self):
        # Find servers own IP address, loop through ethernet devices
        found = False
        address = None
        for dev in ["eth0", "eth1", "eth2"]:
            try:
                address = netifaces.ifaddresses(dev)[netifaces.AF_INET][0]['addr']
                found = True
                break
            except ValueError:
                pass       
        if not found:
            raise Exception("Server address not found")
        return address

    def __setConnTimeout(self):
        timeout = self.__connTimeout
        if timeout < 0:
            # Disable timeout
            self.__connection.setblocking(1)
            #self.__connection.settimeout(None)
        else:
            self.__connection.setblocking(0)
            self.__connection.settimeout(timeout)
    
    def _sendImpl(self, msg):
        self.__connection.sendall(msg)
        
    def _receiveImpl(self, msgSize):
        msg = self.__connection.recv(msgSize) 
        #print "Msg = %s"%msg
        return msg

    def _handleError_Timeout(self, error):
        pass

    def _handleError_Conn(self, error):
        self.closeConnection()
        time.sleep(1)
        #time.sleep(self.__socketTimeout)
        
    def setSocketTimeout(self, timeout):
        super(SocketServer, self).setSocketTimeout(timeout)
        super(SocketServer, self)._setSocketTimeout()
        
    def setConnTimeout(self, timeout):
        self.__connTimeout = float(timeout)
    
    def openConnection(self):
        # Wait for a client to connect, set timeout for listener
        print 'Waiting for incoming client connection...'
        #self.setSocketTimeout(timeout)
        print self.socket
        self.__connection, client_address = self.socket.accept()
        self.__setConnTimeout()
        #self._connection.settimeout(self.__connTimeout)
        self.__remoteAddress, self.__remotePort = client_address
        self._connected = True
        print 'Client connected', client_address
        
    def closeConnection(self):
        if self.__connection:
            self.__connection.close()
        self.__connection = None
        super(SocketServer, self).closeConnection()
        
    def initData(self, initData):
        # Send server init data and receive client init data
        self.sendData(initData)
        return self.receiveData()

