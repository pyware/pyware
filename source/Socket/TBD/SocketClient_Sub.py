#!/usr/bin/python

from Socket.SocketBase  import SocketBase

import time

class SocketClient(SocketBase):
    def __init__(self, address=None, port=10000): 
        SocketBase.__init__(self, address, port)
        
        print 'Socket client started'

    def _sendImpl(self, msg):
        self.socket.sendall(msg)
        
    def _receiveImpl(self, msgSize):
        return self.socket.recv(msgSize)

    def _handleError_Timeout(self, error):
        self.closeConnection()

    def _handleError_Conn(self, error):
        self.closeConnection()
        time.sleep(1)
        #time.sleep(self.__socketTimeout)

    def openConnection(self):
        server_address = (self._address, self._port)
        self._createSocket()
        #print self.socket
        self._setSocketTimeout()
        self.socket.connect(server_address)
        self._connected = True
        print 'Connected to server', server_address
        
    def closeConnection(self):
        self._closeSocket()
        super(SocketClient, self).closeConnection()
        
    def initData(self, initData):
        # Receive server init data and send client init data
        data = self.receiveData()
        self.sendData(initData)
        return data
