#!/usr/bin/python

from Utils.Utils        import Utils
from struct             import unpack, pack

import time, socket, select

class SocketBase(object):
    def __init__(self, address, port):
        
        self._address           = address
        self._port              = port
        self._connected         = False

        self.ignoreConnError    = False
        self.socket             = None
        self.__socketTimeout    = None
        #self._connTimeout     = None
        
        # Set default timeout for socket
        #socket.setdefaulttimeout(10.0)
        #print socket.getdefaulttimeout()

        #self.socket     = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.socket     = None
        #self._createSocket()
        self.utils      = Utils()
                    
    def _createSocket(self):
        if self.socket:
            pass
        else:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def _closeSocket(self):
        if self.socket:
            self.socket.close()
        self.socket = None
    
    def _sendImpl(self, msg):
        # Should be overridden
        raise NotImplementedError
        
    def _receiveImpl(self, msgSize):
        # Should be overridden
        raise NotImplementedError
    
    def __sendMessageCrc(self, msg):
        if msg:
            m = pack('>I', len(msg)) + msg + pack('>I', self.utils.crc32(msg))
            self._sendImpl(m)
            
    def __receiveMessageCrc(self):
        msgSize = self._receiveImpl(4)
        crc_ok = False
        if msgSize:
            msgSize = unpack('>I', str(msgSize))[0]
            msg = self._receiveImpl(msgSize)
            crc_local = self.utils.crc32(msg)
            crc_remote = unpack('>I', str(self._receiveImpl(4)))[0]
            crc_ok = (crc_local == crc_remote)
            if crc_ok:
                return msg
            else:
                raise Exception("Socket CRC missmatch")
        else:
            return None
   
    def _setSocketTimeout(self):
        timeout = self.__socketTimeout
        #if self.socket:
        if timeout < 0:
            # Disable timeout
            self.socket.setblocking(1)
            #self.socket.settimeout(None)
        else:
            self.socket.setblocking(0)
            self.socket.settimeout(timeout)
            
    def _handleError(self, error):
        if self.ignoreConnError:
            try:
                raise error
            except socket.timeout, e:
                print "Warning: %s"%e
                self._handleError_Timeout(e)
            except socket.error, e:
                print "Warning: %s"%e
                self._handleError_Conn(e)
        else:
            raise error

    def _handleError_Timeout(self, error):
        pass

    def _handleError_Conn(self, error):
        pass

    def setSocketTimeout_OLD(self, timeout):
        timeout = float(timeout)
        #if self.socket:
        if timeout < 0:
            # Disable timeout
            self.socket.setblocking(1)
            #self.socket.settimeout(None)
        else:
            self.socket.setblocking(0)
            self.socket.settimeout(timeout)
            
    def setSocketTimeout(self, timeout):
        self.__socketTimeout = float(timeout)
                
    def openConnection(self):
        # Should be overridden
        raise NotImplementedError
        
    def closeConnection(self):
        self._connected = False
        print 'Disconnected'
    
    def close(self):
        self.closeConnection()
        self._closeSocket()
        
    def initData(self, initData):
        # Should be overridden
        raise NotImplementedError

    def sendData(self, data):
        # Packs double data list to a byte string before send
        if data:
            byteStr = self.utils.packList('>d', data)
            self.sendMessage(byteStr)
        
    def receiveData(self):
        # Unpacks received byte string to double data list
        byteStr = self.receiveMessage()
        if byteStr:
            return self.utils.unpackList('>d', byteStr)
        else:
            return None
   
    def receiveMessage(self):
        msg = None
        try:
            if self._connected:
                #print "Try receive"
                msg = self.__receiveMessageCrc()
                if not msg:
                    #print "No msg"
                    self.closeConnection()
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)
        return msg

    def sendMessage(self, msg):
        try:
            if self._connected:
                #print "Try send"
                self.__sendMessageCrc(msg)
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)

    def transferMessage(self, msgOut):
        msgIn = None
        try:
            if self._connected:
                #print "Try transfer"
                self.__sendMessageCrc(msgOut)
                msgIn = self.__receiveMessageCrc()
                if not msgIn:
                    #print "No msg"
                    self.closeConnection()
            else:
                #print "Try connect"
                self.openConnection()
        except Exception, e:
            self._handleError(e)
        return msgIn
