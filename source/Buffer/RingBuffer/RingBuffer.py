#!/usr/bin/python

from collections import deque
import time

class RingBuffer(deque):
    def __init__(self, bufferSize, chunkSize=1):
        # Set buffer size to an even number of chunk sizes
        div, rem = divmod(bufferSize, chunkSize)
        if rem > 0:
            bs = chunkSize*(div+1)
        else:
            bs = bufferSize
        super(RingBuffer, self).__init__(maxlen=bs)
        
        self.__chunkSize = chunkSize
        self.__busy = False
        #print "Actual buffer size %d" %bs
           
    @property
    def average(self):
        if len(self) != 0:
            return float(sum(self))/len(self)
        else:
            return None
            
    @property
    def median(self):
        listSorted  = sorted(list(self))
        listSize    = len(listSorted)
        
        if listSize < 1:
            return None
        elif listSize %2 == 1:
            return float(listSorted[((listSize+1)/2)-1])
        elif listSize %2 == 0:
            return float(sum(listSorted[(listSize/2)-1:(listSize/2)+1]))/2.0
            
    # Not needed, deque should be thread safe
    # If not thread safe, use mutex instead
    """        
    def busy(self):
        while self.__busy:
            time.sleep(0.1)
        self.__busy = True
        
    def done(self):
        self.__busy = False
    """
    
    def __popSize(self, size):
        buffer = []
        if len(self) >= size:
            #self.busy()
            for i in range(size):
                buffer.append(self.popleft())
            #self.done()
            return buffer
        else:
            return None
                
    def popAll(self):
        return self.__popSize(len(self))
                 
    def popChunk(self):
        return self.__popSize(self.__chunkSize)
      
    def popChunkAll(self):
        # Get the remaining number of whole chunks in buffer
        chunks = int(len(self)/self.__chunkSize)
        #print "Chunks to pop %d" %chunks
        return self.__popSize(chunks*self.__chunkSize)
        
    def appendChunk(self, data):
        #self.busy()
        for i in data:
            self.append(i)
        #self.done()
