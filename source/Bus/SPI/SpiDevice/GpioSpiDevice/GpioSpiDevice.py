#!/usr/bin/python

from Bus.SPI.SpiDevice.SpiDevice    import SpiDevice

class GpioSpiDevice(SpiDevice):
    def __init__(self, spi, cs):
        SpiDevice.__init__(self, spi)

        self.__cs   = cs
        self.__mode = spi.getMode()
        
        # Check that CS is valid
        self._spi.checkCs(cs)
        
    def setMode(self, mode):
        # Check that mode is valid and set spi mode property
        if 0 <= mode <= 3:
            self.__mode = mode
        else:
            raise Exception("Error: Mode %d not valid. Select a value between 0 and 3." %mode)
            
    def getMode(self):
        return self.__mode
        
    def __doOperation(self, op, dataW=[], lenR=0):
        dataR = []
        self._spi.doOperation(dataR, self.__mode, self.__cs, op, dataW, lenR)
        # Data is appended to dataR in doOperation, list is by-reference
        return dataR
    
    def readBytes(self, len):
        return self.__doOperation(0, lenR=len)
    
    def writeBytes(self, data):
        self.__doOperation(1, dataW=data)
    
    def xferBytes(self, dataIn):
        return self.__doOperation(2, dataW=dataIn)
        
    def readBlockReg(self, reg, len):
        return self.__doOperation(3, dataW=[reg], lenR=len)
        #return self.__doOperation(4, dataW=[reg], lenR=len)
        
    def writeBlockReg(self, reg, data):
        data.insert(0, reg)
        self.__doOperation(1, dataW=data)
        