#!/usr/bin/python

class SpiDevice(object):
    def __init__(self, spi):
        
        self._spi  = spi
    
    def readBytes(self, len):
        return self._spi.readBytes(len)
    
    def writeBytes(self, data):
        self._spi.writeBytes(data)
    
    def xferBytes(self, data):
        return self._spi.xferBytes(data)
        
    def readBlockReg(self, reg, len):
        self._spi.writeBytes([reg])
        return self._spi.readBytes(len)
        
    def writeBlockReg(self, reg, data):
        data.insert(0, reg)
        self._spi.writeBytes(data)
        
    def read(self):
        # Should be overridden
        raise NotImplementedError
        