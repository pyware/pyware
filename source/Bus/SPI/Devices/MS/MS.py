#!/usr/bin/python
 
from Bus.SPI.SpiEngine.SpiEngine    import SpiEngine
from Utils.Utils                    import Utils

class MS(SpiEngine):
    def __init__(self, bus, device):
        SpiEngine.__init__(self, bus, device)
        
        self.utils  = Utils()
        
        self.__status           = 0
        self.__pressureCount    = 0
        self.__tempCount        = 0
        
        self.__pressure         = 0
        self.__temp             = 0

    # Read status, pressure and temp data
    def read(self):
        
        # Read 4 bytes and merge into 1 U32 then convert to bit string
        data         = self.readBytes(4)
        raw_u32      = sum(data[3-i] << (i * 8) for i in range(4))
        raw_bit_str  = str('{:032b}'.format(raw_u32))  
        
        # Split bit string and convert to 3 integers
        self.__status          = int(raw_bit_str[0:2],2)
        self.__pressureCount   = int(raw_bit_str[2:16],2)
        self.__tempCount       = int(raw_bit_str[16:27],2)
        
        # Convert count values into pressure and temp
        self.__pressure  = self._convertPressure(self.__pressureCount)
        self.__temp      = self._convertTemp(self.__tempCount)
        
        return [self.__status, self.__pressure, self.__temp]
     
    def setSpeed(self, speed):
        self.msh = speed
        
    # Print out results    
    def outputText(self):
        print("--------------------------------------------")
        print("Status   : %d" %self.__status)
        print("Pressure : %.2f mmHg (%d counts)" %(self.__pressure, self.__pressureCount))
        print("Temp     : %.2f deg C (%d counts)" %(self.__temp, self.__tempCount))

    # Convert count to pressure
    def _convertPressure(self, count):
        # Should be overridden
        raise NotImplementedError
        
    # Convert count to temp
    def _convertTemp(self, count):
        maxIn = 2047
        minIn = 0
        maxOut = 150
        minOut = -50

        return self.utils.scale(count, minIn, maxIn, minOut, maxOut)
        