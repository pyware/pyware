#!/usr/bin/python

from Bus.SPI.Devices.MS.MS     import MS

# Reference pressure sensor 015V
class MS_86BSD_015V(MS):
    def __init__(self, bus, device):
        MS.__init__(self, bus, device)

    # Convert count to pressure
    def _convertPressure(self, count):
        maxIn = 14745
        minIn = 1638
        maxOut = 750
        minOut = -700

        return self.utils.scale(count, minIn, maxIn, minOut, maxOut)
    
