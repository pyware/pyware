#!/usr/bin/python

from Bus.SPI.Devices.MS.MS     import MS

# Absolute pressure sensor 030A
class MS_86BSD_030A(MS):
    def __init__(self, bus, device):
        MS.__init__(self, bus, device)

    # Convert count to pressure
    def _convertPressure(self, count):
        maxIn = 14745
        minIn = 1638
        maxOut = 1500
        minOut = 50

        return self.utils.scale(count, minIn, maxIn, minOut, maxOut)
     
