#!/usr/bin/python
# coding=utf-8

from Bus.SPI.SpiDevice.GpioSpiDevice.GpioSpiDevice      import GpioSpiDevice
from Utils.Utils                                        import Utils
from struct import unpack, pack
import time, math

class IS_MPU9250(GpioSpiDevice):

    # Registers for Accel and Gyro
    REG_SMPLRT_DIV      = 0x19
    REG_CONFIG          = 0x1A
    REG_GYRO_CONFIG     = 0x1B
    REG_ACCEL_CONFIG    = 0x1C
    REG_ACCEL_CONFIG_2  = 0x1D
    REG_FIFO_EN         = 0x23

    REG_I2C_MST_CTRL    = 0x24
    REG_I2C_SLV0_ADDR   = 0x25
    REG_I2C_SLV0_REG    = 0x26
    REG_I2C_SLV0_CTRL   = 0x27    
    REG_I2C_SLV0_DO     = 0x63
    REG_EXT_SENS_DATA_00 = 0x49

    REG_ACCEL_XOUT_H    = 0x3B
    REG_ACCEL_XOUT_L    = 0x3C
    REG_ACCEL_YOUT_H    = 0x3D
    REG_ACCEL_YOUT_L    = 0x3E
    REG_ACCEL_ZOUT_H    = 0x3F
    REG_ACCEL_ZOUT_L    = 0x40

    REG_TEMP_OUT_H      = 0x41
    REG_TEMP_OUT_L      = 0x42

    REG_GYRO_XOUT_H     = 0x43
    REG_GYRO_XOUT_L     = 0x44
    REG_GYRO_YOUT_H     = 0x45
    REG_GYRO_YOUT_L     = 0x46
    REG_GYRO_ZOUT_H     = 0x47
    REG_GYRO_ZOUT_L     = 0x48
    
    REG_USER_CTRL       = 0x6A
    REG_PWR_MGMT_1      = 0x6B
    REG_WHO_AM_I        = 0x75
    
    # Registers for Magnetometer
    REG_MAG_DEVID       = 0x00
    REG_MAG_INFO		= 0x01
    REG_MAG_ST1			= 0x02
    REG_MAG_HXL         = 0x03
    REG_MAG_HXH         = 0x04
    REG_MAG_HYL         = 0x05
    REG_MAG_HYH         = 0x06
    REG_MAG_HZL         = 0x07
    REG_MAG_HZH         = 0x08
    REG_MAG_ST2			= 0x09
    REG_MAG_CTRL1       = 0x0A
    
    # Read/Write
    OP_WRITE            = 0x00
    OP_READ             = 0x01

    # Misc
    MISC_MAG_I2C_ADDR   = 0x0C
    MISC_CHIP_ID        = 0x71
    MISC_MAG_CHIP_ID    = 0x48
    MISC_ACCEL_SCALES   = [2, 4, 8, 16] # g
    MISC_GYRO_SCALES    = [250, 500, 1000, 2000] # deg/s
    MISC_MAG_SCALES     = [4912] # uT
    
    def __init__(self, spi, cs):
        GpioSpiDevice.__init__(self, spi, cs)

        self.__accelScale   = -1
        self.__gyroScale    = -1
        self.__magScale     = self.MISC_MAG_SCALES[0]
        self.__i2cSlv0_NumBytes  = -1
        
    def __formatCmd(self, reg, rw):
        #if self.MC_MCP3903_REG_CH0 <= reg <= self.MC_MCP3903_REG_CONFIG:
        if 0 <= reg <= 126:
            cmd = (rw << 7) + reg
            #print "CMD: %s" %cmd
            return cmd
        else:
            raise Exception("Error: Register %d given. Expected value 0-126." %reg)
        
    def __sendReadCmd(self, reg, numBytes=1):
        cmd = self.__formatCmd(reg, self.OP_READ)
        # Read bytes from specified register. If numBytes>1 the next registers are read sequentially.
        bytes = self.readBlockReg(cmd, numBytes)
        #print "Read: %s" %bytes
        #return self.__mergeData(bytes)
        return bytes
    
    def __sendWriteCmd(self, reg, data):
        cmd = self.__formatCmd(reg, self.OP_WRITE)
        #bytes = self.__splitData(data)
        bytes = [data]
        #print "Write: %s" %bytes
        self.writeBlockReg(cmd, bytes) # Write data bytes to specified register

    def __mergeData(self, msb, lsb):
        # Merge 2 bytes into 1 int16
        uint16 = pack('H', (msb<<8) + lsb)
        int16 = unpack('<h', uint16)[0]
        return int16

    def __scaleData(self, rawData, scale):
        # Scale raw int16 value to actual value
        return float(rawData) / pow(2, 16-1) * scale
        
    def __i2cSlv0_ConfigRead(self, addr, reg, numBytes=1, disReg=0):
        if 1 <= numBytes <= 16:
            self.__i2cSlv0_NumBytes = numBytes
            self.__sendWriteCmd(self.REG_I2C_SLV0_ADDR, (self.OP_READ << 7) + addr)
            self.__sendWriteCmd(self.REG_I2C_SLV0_REG, reg)
            self.__sendWriteCmd(self.REG_I2C_SLV0_CTRL, (1 << 7) + (disReg << 5) + numBytes)
            #time.sleep(0.1)
        else:
            raise Exception("Error: I2C read, num bytes %d given. Expected value 1-16." %numBytes)
        
    def __i2cSlv0_ReadData(self):
        if self.__i2cSlv0_NumBytes:
            return self.__sendReadCmd(self.REG_EXT_SENS_DATA_00, self.__i2cSlv0_NumBytes)
        else:
            raise Exception("Error: I2C read, config invalid. Execute method __i2cSlv0_ConfigRead first.")

    def __i2cSlv0_WriteData(self, addr, reg, data, disReg=0):
        # Not sure about the order of cmds, but this seems to work best.
        self.__sendWriteCmd(self.REG_I2C_SLV0_ADDR, (self.OP_WRITE << 7) + addr)
        self.__sendWriteCmd(self.REG_I2C_SLV0_REG, reg)
        self.__sendWriteCmd(self.REG_I2C_SLV0_DO, data)
        self.__sendWriteCmd(self.REG_I2C_SLV0_CTRL, (1 << 7) + (disReg << 5) + 1)
        #time.sleep(0.1)

    """
    def __mergeData(self, bytes):
        # Merge byte array into 1 uint
        data = 0
        b = bytes[::-1] # Reverse array
        for i in range(len(b)):
            data += (b[i] << (i*8))
        return data
        
    def __splitData(self, data):
        bytes = []
        # Split uint32 (I) into 4 uint8 bytes, byte order = little-endian (<)
        b = pack('<I', data)
        for i in range(self.MISC_DATA_SIZE): # Ignore last byte (24 bits total)
            bytes.append(unpack('B', b[i])[0])
        return bytes[::-1] # Reverse array
    """
        
    def setup(self):
        # Startup device
        self.setSleep(False)        # Wake up device
        time.sleep(0.2)             # Wait for startup
        
        # Check device ID
        id = self.getId()
        if id != self.MISC_CHIP_ID:
            raise Exception("Error: Chip ID %d read. Expected value %d." %(id, self.MISC_CHIP_ID))
            #pass

        # Default settings
        self.setUserConfig(1 << 5)  # I2C Slave OFF (SPI ON), I2C Master Mode ON, FIFO OFF
        self.setSampleRateDiv(0)    # No sample rate divider
        self.setAccelScale(0)       # Highest sensitivity
        self.setGyroScale(0, True)  # Highest sensitivity, Convert to rad/s
        self.setAccelFilter(0)      # Fast filter
        self.setGyroFilter(0)       # Fast filter
        
        # Startup magnetometer
        self.setMagSleep(False)     # Wake up magnetometer
        time.sleep(0.2)             # Wait for startup

    def cleanUp(self):
        self.setMagSleep(True)  	# Set magnetometer to sleep
        self.setSleep(True)        	# Set device to sleep

    def getId(self):
        return self.__sendReadCmd(self.REG_WHO_AM_I)[0]

    def setSleep(self, sleep=False):
        if sleep:
            value = 1 << 6
        else:
            value = 0
        # Set sleep bit in power reg (bit 6). Also set auto select available clock source (bit 0-2).
        self.__sendWriteCmd(self.REG_PWR_MGMT_1, value + 1)

    def setUserConfig(self, config):
        if 0 <= config <= 255:
            self.__sendWriteCmd(self.REG_USER_CTRL, config)
        else:
            raise Exception("Error: User config %d given. Expected value 0-255." %config)

    def setFifo(self, config):
        # Sets FIFO config.
        # Note that FIFO_EN must also be set in setUserConfig.
        if 0 <= config <= 255:
            self.__sendWriteCmd(self.REG_FIFO_EN, config)
        else:
            raise Exception("Error: FIFO config %d given. Expected value 0-255." %config)

    def getSampleRateDiv(self):
        return self.__sendReadCmd(self.REG_SMPLRT_DIV)[0]

    def setSampleRateDiv(self, sr_div):
        # SAMPLE_RATE = Internal_Sample_Rate / (1 + sr_div)
        if 0 <= sr_div <= 255:
            self.__sendWriteCmd(self.REG_SMPLRT_DIV, sr_div)
        else:
            raise Exception("Error: Sample rate divider %d given. Expected value 0-255." %sr_div)

    def getAccelScale(self):
        data = self.__sendReadCmd(self.REG_ACCEL_CONFIG)[0]
        # Only pick bit 3-4 for scale
        return (data & 0b00011000) >> 3

    def setAccelScale(self, scale):
        # Accel Full Scale Select: +-2g (0), +-4g (1), +-8g (2), +-16g (3)
        if 0 <= scale <= 3:
            self.__sendWriteCmd(self.REG_ACCEL_CONFIG, scale << 3)
            self.__accelScale = self.MISC_ACCEL_SCALES[scale]
        else:
            raise Exception("Error: Accelerometer scale %d given. Expected value 0-3." %scale)

    def getGyroScale(self):
        data = self.__sendReadCmd(self.REG_GYRO_CONFIG)[0]
        # Only pick bit 3-4 for scale
        return (data & 0b00011000) >> 3

    def setGyroScale(self, scale, convToRad=False):
        # Gyro Full Scale Select: +250dps (0), +500dps (1), +1000dps (2), +2000dps (3)
        # Also set FCHOICE_B to 0 to enable filter (FCHOICE = !FCHOICE_B)
        if 0 <= scale <= 3:
            self.__sendWriteCmd(self.REG_GYRO_CONFIG, scale << 3)
            if convToRad: # rad/s
                self.__gyroScale = math.radians(self.MISC_GYRO_SCALES[scale])
            else: # deg/s
                self.__gyroScale = self.MISC_GYRO_SCALES[scale]
        else:
            raise Exception("Error: Gyro scale %d given. Expected value 0-3." %scale)

    def getAccelFilter(self):
        data = self.__sendReadCmd(self.REG_ACCEL_CONFIG_2)[0]
        # Only pick bit 0-2 for filter data
        return (data & 0b00000111)

    def setAccelFilter(self, filter):
        '''
        Sets the accel low pass filter cutoff frequency.
        Filter:             0   1   2   3   4   5   6   7
        Cutoff (Hz):        460 184 92  41  20  10  5   460
        Sample rate (KHz):  1   1   1   1   1   1   1   1
        Also sets ACCEL_FCHOICE_B to 0 to enable filter (ACCEL_FCHOICE = !ACCEL_FCHOICE_B)
        '''
        if 0 <= filter <= 7:
            self.__sendWriteCmd(self.REG_ACCEL_CONFIG_2, filter)
        else:
            raise Exception("Error: Accelerometer filter %d given. Expected value 0-7." %filter)

    def getGyroFilter(self):
        data = self.__sendReadCmd(self.REG_CONFIG)[0]
        # Only pick bit 0-2 for filter data
        return (data & 0b00000111)

    def setGyroFilter(self, filter):
        '''
        Sets the gyro and temperature sensor low pass filter cutoff frequency.
        Filter:             0   1   2   3   4   5   6   7
        Cutoff (Hz):        250 184 92  41  20  10  5   3600
        Sample rate (KHz):  8   1   1   1   1   1   1   8
        '''
        if 0 <= filter <= 7:
            self.__sendWriteCmd(self.REG_CONFIG, filter)
        else:
            raise Exception("Error: Gyro filter %d given. Expected value 0-7." %filter)

    def setMagSleep(self, sleep=False):
        if sleep:
            # Power down mode
            value = 0b0000
        else:
            # Continuous measurement mode 2
            value = 0b0110
        # Set bit 0-3 for power down mode or cont meas mode. Also set bit 4 for 16 bit mode.
        self.__i2cSlv0_WriteData(self.MISC_MAG_I2C_ADDR, self.REG_MAG_CTRL1, (1 << 4) + value)

    def getTemp(self):
        # TEMP_degC = ((TEMP_OUT - RoomTemp_Offset)/Temp_Sensitivity) + 21degC
        # Read 2 bytes for temp H and L
        data = self.__sendReadCmd(self.REG_TEMP_OUT_H, 2)
        return (self.__mergeData(data[0], data[1]) - 0)/333.87 + 21

    def getAccel(self):
        if self.__accelScale >= 0:
            # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
            data = self.__sendReadCmd(self.REG_ACCEL_XOUT_H, 6)
            x = self.__scaleData(self.__mergeData(data[0], data[1]), self.__accelScale)
            y = self.__scaleData(self.__mergeData(data[2], data[3]), self.__accelScale)
            z = self.__scaleData(self.__mergeData(data[4], data[5]), self.__accelScale)
            return [x,y,z]
        else:
            raise Exception("Error: Accel scale has not been set. Use setup or setAccelScale method.")

    def getGyro(self):
        if self.__gyroScale >= 0:
            # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
            data = self.__sendReadCmd(self.REG_GYRO_XOUT_H, 6)
            x = self.__scaleData(self.__mergeData(data[0], data[1]), self.__gyroScale)
            y = self.__scaleData(self.__mergeData(data[2], data[3]), self.__gyroScale)
            z = self.__scaleData(self.__mergeData(data[4], data[5]), self.__gyroScale)
            return [x,y,z]
        else:
            raise Exception("Error: Gyro scale has not been set. Use setup or setGyroScale method.")

    def getMag(self):
        if self.__magScale >= 0:
            # Read 8 bytes for ST1, XH, XL, YH, YL, ZH, ZL, ST2. (Must read ST1 and ST2 to get data.)
            self.__i2cSlv0_ConfigRead(self.MISC_MAG_I2C_ADDR, self.REG_MAG_ST1, 8)
            data = self.__i2cSlv0_ReadData()[1:7] # Skip ST1, ST2 bytes
            x = self.__scaleData(self.__mergeData(data[1], data[0]), self.__magScale)
            y = self.__scaleData(self.__mergeData(data[3], data[2]), self.__magScale)
            z = self.__scaleData(self.__mergeData(data[5], data[4]), self.__magScale)
            return [x,y,z]
        else:
            raise Exception("Error: Mag scale has not been set.")
            
    def testRead(self):
    	self.__i2cSlv0_ConfigRead(self.MISC_MAG_I2C_ADDR, self.REG_MAG_CTRL1, 1)
    	print self.__i2cSlv0_ReadData()
