#!/usr/bin/python

from Bus.SPI.SpiDevice.GpioSpiDevice.GpioSpiDevice   import GpioSpiDevice
from Utils.Utils                                    import Utils
from struct import unpack, pack
import time

class AD_AD5724R(GpioSpiDevice):
    
    # Bit resolution
    AD_AD5724R_BIT_RES          = 12
    
    # Registers
    AD_AD5724R_DAC_REG          = 0b000
    AD_AD5724R_RANGE_REG        = 0b001
    AD_AD5724R_POWER_REG        = 0b010
    AD_AD5724R_CONTROL_REG      = 0b011
    
    # Channels
    AD_AD5724R_CH_A             = 0b000
    AD_AD5724R_CH_B             = 0b001
    AD_AD5724R_CH_C             = 0b010
    AD_AD5724R_CH_D             = 0b011
    AD_AD5724R_CH_ALL           = 0b100
    
    # Ranges
    AD_AD5724R_RANGE_P5         = 0b000
    AD_AD5724R_RANGE_P10        = 0b001
    AD_AD5724R_RANGE_P108       = 0b010
    AD_AD5724R_RANGE_PM5        = 0b011
    AD_AD5724R_RANGE_PM10       = 0b100
    AD_AD5724R_RANGE_PM108      = 0b101
    
    # Control
    AD_AD5724R_CONTROL_NOP      = 0b000
    AD_AD5724R_CONTROL_SETUP    = 0b001
    AD_AD5724R_CONTROL_CLEAR    = 0b100
    AD_AD5724R_CONTROL_LOAD     = 0b101
    
    # R/W
    AD_AD5724R_WRITE            = 0b0
    AD_AD5724R_READ_WRITE       = 0b1
    
    # Power bits
    AD_AD5724R_PU_A             = 0b00001
    AD_AD5724R_PU_B             = 0b00010
    AD_AD5724R_PU_C             = 0b00100
    AD_AD5724R_PU_D             = 0b01000
    AD_AD5724R_PU_ALL           = 0b01111
    AD_AD5724R_PU_REF           = 0b10000
    
    # Coding types
    AD_AD5724R_2COMP_CODE       = 0
    AD_AD5724R_BIN_CODE         = 1
    
    # Reference voltage types
    AD_AD5724R_EXT_REF          = 0
    AD_AD5724R_INT_REF          = 1
    
    # Internal ref voltage
    AD_AD5724R_INT_REF_VOLT     = 2.5
    
    # Misc
    AD_AD5724R_NUM_CHS          = 4

    def __init__(self, spi, cs):
        GpioSpiDevice.__init__(self, spi, cs)

        self.__refType  = self.AD_AD5724R_EXT_REF # Use ext ref by default to be safe
        self.__refVolt  = 0
        self.__coding   = self.AD_AD5724R_2COMP_CODE
        self.__range    = self.AD_AD5724R_RANGE_PM10
        
        self.__chs      = [self.AD_AD5724R_CH_A, self.AD_AD5724R_CH_B, 
                           self.AD_AD5724R_CH_C, self.AD_AD5724R_CH_D, 
                           self.AD_AD5724R_CH_ALL]
        self.__puChs    = [self.AD_AD5724R_PU_A, self.AD_AD5724R_PU_B, 
                           self.AD_AD5724R_PU_C, self.AD_AD5724R_PU_D, 
                           self.AD_AD5724R_PU_ALL]
        
    def __formatCmd(self, rw, reg, ch, data1, data0):
        data = (data1 << 8) + data0
        if 0 <= data < pow(2,16):
            b1 = (rw << 7) + (reg << 3) + ch
            b2 = data1
            b3 = data0
            return [b1,b2,b3] # MSB first
        else:
            raise Exception("Error: Input data value %d given. Expected value 0-65535 (2^16 - 1)." %data)
        
    def __sendWriteCmd(self, reg, ch, data1, data0):
        cmd = self.__formatCmd(self.AD_AD5724R_WRITE, reg, ch, data1, data0)
        self.writeBytes(cmd)
        
    def __sendReadCmd(self, reg, ch):
        cmd1 = self.__formatCmd(self.AD_AD5724R_READ_WRITE, reg, ch, 0, 0) # Data bits are dont care
        cmd2 = self.__formatCmd(self.AD_AD5724R_WRITE, self.AD_AD5724R_CONTROL_REG, self.AD_AD5724R_CONTROL_NOP, 0, 0)
        self.writeBytes(cmd1)           # First write cmd for what to read
        return self.xferBytes(cmd2)     # Then xfer NOP cmd to read the wanted data
        #return self.writeXferBytes(cmd1, cmd2)
    
    def __splitData(self, data):
        # Split int16 (h) into 2 uint8 (B), byte order = little-endian (<)
        bytes = pack('<h', data)
        data0 = unpack('B', bytes[0])[0]
        data1 = unpack('B', bytes[1])[0]
        return data1, data0
        
    def __mergeData(self, data1, data0):
        data = (data1 << 8) + data0
        return data
        
    def __splitDacData(self, data):
        # Shift to ignore LSB bits for lower resolutions (16 bits is max for this series)
        data = data << (16 - self.AD_AD5724R_BIT_RES)
        return self.__splitData(data)
        
    def __mergeDacData(self, data1, data0):
        data = self.__mergeData(data1, data0)
        # Pack data as uint16, then unpack as int16
        bytes = pack('<H', data)
        value = unpack('<h', bytes)[0]
        # Shift to ignore LSB bits for lower resolutions
        value = value >> (16 - self.AD_AD5724R_BIT_RES)
        return value
    
    def __setDacBin(self, ch, data):
        if 0 <= data < pow(2,self.AD_AD5724R_BIT_RES):
            d1, d0 = self.__splitDacData(data)
            self.__sendWriteCmd(self.AD_AD5724R_DAC_REG, ch, d1, d0)
        else:
            raise Exception("Error: DAC value %d given. Expected value 0-4095 (2^12 - 1) for binary coding." %data)
            
    def __setDac2Comp(self, ch, data):
        amp = pow(2,self.AD_AD5724R_BIT_RES-1)
        if -(amp+1) <= data < amp:
            d1, d0 = self.__splitDacData(data)
            self.__sendWriteCmd(self.AD_AD5724R_DAC_REG, ch, d1, d0)
        else:
            raise Exception("Error: DAC value %d given. Expected value -2048 to +2047 (2^11 - 1) for 2s-comp coding." %data)
    
    def __checkVoltage(self, voltage, min, max):
        if min <= voltage <= max:
            pass
        else:
            raise Exception("Error: Voltage %f given. Expected value %.1f to %.1f V." %(voltage, min, max))
    
    def setup(self):
        # Set defaults
        self.clearDac()
        time.sleep(0.01)
        self.setRef(self.__refType)
        time.sleep(0.01)
        self.setRange(self.AD_AD5724R_CH_ALL, self.__range)
        time.sleep(0.01)
        self.setPower(self.AD_AD5724R_CH_ALL, 1) # Power on
        time.sleep(0.01)
        
    def cleanUp(self):
        self.clearDac()
        #self.setDac(self.AD_AD5724R_CH_ALL, 0)
        self.setPower(self.AD_AD5724R_CH_ALL, 0) # Power off
        
    def setDac(self, ch, data):
        if ch in self.__chs:
            if self.__coding:
                self.__setDacBin(self.__chs[ch], data)
            else:
                self.__setDac2Comp(self.__chs[ch], data)
        else:
            raise Exception("Error: Channel %d given. Expected value 0-4 (0=A, 1=B, 2=C, 3=D, 4=ALL)." %ch)
            
    def getDac(self, ch):
        rb = self.__sendReadCmd(self.AD_AD5724R_DAC_REG, self.__chs[ch])
        data1, data0 = rb[1], rb[2]
        return self.__mergeDacData(data1, data0)

    def loadDac(self):
        self.__sendWriteCmd(self.AD_AD5724R_CONTROL_REG, self.AD_AD5724R_CONTROL_LOAD, 0, 0)
        
    def clearDac(self):
        self.__sendWriteCmd(self.AD_AD5724R_CONTROL_REG, self.AD_AD5724R_CONTROL_CLEAR, 0, 0)   
    
    def setVoltage(self, ch, voltage):
        voltage = float(voltage)
        # For unipolar (and bipolar with binary offset coding) the count value is between 0 and (2^12)-1.
        # For bipolar with 2s-comp coding the count value is between -(2^11)-1 and (2^11).
        maxCountUni = pow(2, self.AD_AD5724R_BIT_RES) - 1
        maxCountPos = pow(2, self.AD_AD5724R_BIT_RES-1) - 1
        maxCountNeg = pow(2, self.AD_AD5724R_BIT_RES-1)
        
        if self.__refVolt == 0:
            raise Exception("Error: Reference voltage has not been specified. Use setRef method.")
        
        if self.__range == self.AD_AD5724R_RANGE_P5:
            self.__checkVoltage(voltage, 0, 5)
            gain = 2.0
            count = voltage / (gain * self.__refVolt) * maxCountUni
                
        elif self.__range == self.AD_AD5724R_RANGE_P10:
            self.__checkVoltage(voltage, 0, 10)
            gain = 4.0
            count = voltage / (gain * self.__refVolt) * maxCountUni
                
        elif self.__range == self.AD_AD5724R_RANGE_P108:
            self.__checkVoltage(voltage, 0, 10.8)
            gain = 4.32
            count = voltage / (gain * self.__refVolt) * maxCountUni
                
        elif self.__range == self.AD_AD5724R_RANGE_PM5:
            self.__checkVoltage(voltage, -5, 5)
            gain = 2.0
            if self.__coding == self.AD_AD5724R_2COMP_CODE: # 2s-comp coding
                if voltage < 0:
                    count = voltage / (gain * self.__refVolt) * maxCountNeg
                else:
                    count = voltage / (gain * self.__refVolt) * maxCountPos
            else: # If AD_AD5724R_BIN_CODE (binary offset coding)
                gain = gain * 2
                count = (voltage / (gain * self.__refVolt) + 0.5) * maxCountUni
        
        elif self.__range == self.AD_AD5724R_RANGE_PM10:
            self.__checkVoltage(voltage, -10, 10)
            gain = 4.0
            if self.__coding == self.AD_AD5724R_2COMP_CODE: # 2s-comp coding
                if voltage < 0:
                    count = voltage / (gain * self.__refVolt) * maxCountNeg
                else:
                    count = voltage / (gain * self.__refVolt) * maxCountPos
            else: # If AD_AD5724R_BIN_CODE (binary offset coding)
                gain = gain * 2
                count = (voltage / (gain * self.__refVolt) + 0.5) * maxCountUni
                
        elif self.__range == self.AD_AD5724R_RANGE_PM108:
            self.__checkVoltage(voltage, -10.8, 10.8)
            gain = 4.32
            if self.__coding == self.AD_AD5724R_2COMP_CODE: # 2s-comp coding
                if voltage < 0:
                    count = voltage / (gain * self.__refVolt) * maxCountNeg
                else:
                    count = voltage / (gain * self.__refVolt) * maxCountPos
            else: # If AD_AD5724R_BIN_CODE (binary offset coding)
                gain = gain * 2
                count = (voltage / (gain * self.__refVolt) + 0.5) * maxCountUni

        data = int(round(count))        
        self.setDac(ch, data)
        #print "Voltage = %f " %voltage
        #print "Count = %f " %count
        #print "Data = %d " %data
        #print "Ch = %d " %ch
    
    def setCoding(self, coding):
        if coding in [self.AD_AD5724R_2COMP_CODE, self.AD_AD5724R_BIN_CODE]:
            self.__coding = coding
        else:
            raise Exception("Error: Coding value %d given. Expected value 0-1." %coding)

    def setRange(self, ch, range):
        if self.AD_AD5724R_RANGE_P5 <= range <= self.AD_AD5724R_RANGE_PM108:
            self.__sendWriteCmd(self.AD_AD5724R_RANGE_REG, self.__chs[ch], 0, range)
            self.__range = range
        else:
            raise Exception("Error: Range value %d given. Expected value 0-5." %range)
    
    def getRange(self, ch):
        rb = self.__sendReadCmd(self.AD_AD5724R_RANGE_REG, self.__chs[ch])
        return self.__mergeData(rb[1], rb[2])
    
    def setRef(self, refType, voltage=0):
        # Only set given voltage if external ref type, otherwise use internal default voltage (2.5 V).
        if refType in [0,1]:
            data0 = refType and self.AD_AD5724R_PU_REF
            self.__sendWriteCmd(self.AD_AD5724R_POWER_REG, 0, 0, data0)
            self.__refType = refType
            if refType:
                self.__refVolt = self.AD_AD5724R_INT_REF_VOLT
            else:
                self.__refVolt = voltage
        else:
            raise Exception("Error: Reference type %d given. Expected 0 or 1 (external or internal)." %ref) 
    
    def setPower(self, ch, powerOn):
        if powerOn in [0,1]:
            data0 = (self.__refType and self.AD_AD5724R_PU_REF) + (self.__puChs[ch] * powerOn)
            self.__sendWriteCmd(self.AD_AD5724R_POWER_REG, 0, 0, data0)
        else:
            raise Exception("Error: Power on value %d given. Expected value 0 or 1 (off or on)." %powerOn)
            
    def getPower(self):
        rb = self.__sendReadCmd(self.AD_AD5724R_POWER_REG, 0)
        return self.__mergeData(rb[1], rb[2])
        
    def setControl(self, data):
        self.__sendWriteCmd(self.AD_AD5724R_CONTROL_REG, self.AD_AD5724R_CONTROL_SETUP, 0, data)
        
    def getControl(self):
        rb = self.__sendReadCmd(self.AD_AD5724R_CONTROL_REG, self.AD_AD5724R_CONTROL_SETUP)
        return self.__mergeData(rb[1], rb[2])
        