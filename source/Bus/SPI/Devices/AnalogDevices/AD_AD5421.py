#!/usr/bin/python

from Bus.SPI.SpiDevice.GpioSpiDevice.GpioSpiDevice   import GpioSpiDevice
from Utils.Utils                                    import Utils
from struct import unpack, pack
import time

class AD_AD5421(GpioSpiDevice):
    
    # Bit resolution
    AD_AD5421_BIT_RES           = 16
    
    # Registers
    AD_AD5421_W_DAC_REG         = 0b00000001
    AD_AD5421_W_CONTR_REG       = 0b00000010
    AD_AD5421_W_OFFS_REG        = 0b00000011
    AD_AD5421_W_GAIN_REG        = 0b00000100
    
    AD_AD5421_R_DAC_REG         = 0b10000001
    AD_AD5421_R_CONTR_REG       = 0b10000010
    AD_AD5421_R_OFFS_REG        = 0b10000011
    AD_AD5421_R_GAIN_REG        = 0b10000100
    AD_AD5421_R_FAULT_REG       = 0b10000101

    # Commands
    AD_AD5421_LOAD_DAC          = 0b00000101
    AD_AD5421_FORCE_ALARM       = 0b00000110
    AD_AD5421_RESET             = 0b00000111
    AD_AD5421_INIT_VT_MEAS      = 0b00001000
    AD_AD5421_NOP               = 0b00001001
    
    # Watchdog timeouts (ms)
    AD_AD5421_WD_50             = 0b000
    AD_AD5421_WD_100            = 0b001
    AD_AD5421_WD_500            = 0b010
    AD_AD5421_WD_1000           = 0b011 # Default
    AD_AD5421_WD_2000           = 0b100
    AD_AD5421_WD_3000           = 0b101
    AD_AD5421_WD_4000           = 0b110
    AD_AD5421_WD_5000           = 0b111
    
    # Ranges (mA)
    AD_AD5421_RANGE_40_200      = [4.0, 20.0]
    AD_AD5421_RANGE_38_210      = [3.8, 21.0]
    AD_AD5421_RANGE_32_240      = [3.2, 24.0]
    
    # Voltages (V)
    AD_AD5421_V_REFOUT1         = 2.5
    AD_AD5421_V_REFOUT2         = 1.22
    
    AD_AD5421_V_REGOUT_18       = 1.8
    AD_AD5421_V_REGOUT_25       = 2.5
    AD_AD5421_V_REGOUT_30       = 3.0
    AD_AD5421_V_REGOUT_33       = 3.3
    AD_AD5421_V_REGOUT_50       = 5.0
    AD_AD5421_V_REGOUT_90       = 9.0
    AD_AD5421_V_REGOUT_120      = 12.0
    
    # ADC inputs
    AD_AD5421_ADC_VOLT          = 0
    AD_AD5421_ADC_TEMP          = 1

    def __init__(self, spi, cs):
        GpioSpiDevice.__init__(self, spi, cs)

        self.__ranges   = [self.AD_AD5421_RANGE_40_200, self.AD_AD5421_RANGE_38_210, self.AD_AD5421_RANGE_32_240]
        self.__rangeIdx = -1
            
    def __formatCmd(self, reg, data):
        if 0 <= data < pow(2,self.AD_AD5421_BIT_RES):
            data1, data0 = self.__splitData(data)
            cmd = [reg,data1,data0]     # MSB first
            #print "Cmd: %s" %cmd 
            return cmd
        else:
            raise Exception("Error: Input data value %d given. Expected value 0-65535 (2^16 - 1)." %data)
        
    def __sendWriteCmd(self, reg, data):
        cmd = self.__formatCmd(reg, data)
        fault = self.xferBytes(cmd)     # Fault register is returned if enabled
        #print "Fault W: %s" %fault
        return fault
        
    def __sendReadCmd(self, reg, data):
        cmd1 = self.__formatCmd(reg, data)
        cmd2 = self.__formatCmd(self.AD_AD5421_NOP, 0)
        fault = self.xferBytes(cmd1)    # First write cmd for what to read
        #print "Fault R: %s" %fault
        return self.xferBytes(cmd2)     # Then xfer NOP cmd to read the wanted data
    
    def __mergeData(self, data1, data0):
        data = (data1 << 8) + data0
        return data
    
    def __splitData(self, data):
        # Split uint16 (H) into 2 uint8 (B), byte order = little-endian (<)
        bytes = pack('<H', data)
        data0 = unpack('B', bytes[0])[0]
        data1 = unpack('B', bytes[1])[0]
        return data1, data0
    
    def setup(self):
        # Set defaults
        self.setControl(wdTimeout=self.AD_AD5421_WD_5000, faultRbDisable=1)
        time.sleep(0.1)
        self.setDac(0)
        time.sleep(0.1)
        
    def cleanUp(self):
        self.setDac(0)
        
    def setDac(self, data):
        self.__sendWriteCmd(self.AD_AD5421_W_DAC_REG, data)
            
    def getDac(self):
        readback = self.__sendReadCmd(self.AD_AD5421_R_DAC_REG, 0)
        data1, data0 = readback[1], readback[2]
        return self.__mergeData(data1, data0)

    def setRange(self, rangeIdx):
        if rangeIdx in range(0,len(self.__ranges)):
            self.__rangeIdx = rangeIdx
        else:
            raise Exception("Error: Range index %d given. Expected value 0-3." %rangeIdx)
    
    def getRange(self):
        return self.__ranges[self.__rangeIdx]
    
    def setCurrent(self, current):
        current     = float(current)
        countTot    = pow(2, self.AD_AD5421_BIT_RES) - 1
        count       = 0
        
        if self.__rangeIdx == -1:
            raise Exception("Error: Current range has not been specified. Use setRange method.")
        
        minRange = self.__ranges[self.__rangeIdx][0]
        maxRange = self.__ranges[self.__rangeIdx][1]
        
        if minRange <= current <= maxRange:
            count = (current - minRange) / (maxRange - minRange) * countTot
        else:
        #    raise Exception("Error: Current %f given. Expected value %f - %f." %(current, minRange, maxRange))
            print "Warning: Current %f given. Expected value %f - %f." %(current, minRange, maxRange)

        data = int(round(count))        
        self.setDac(data)
        #print "Current = %f " %current
        #print "Count = %f " %count
        #print "Data = %d " %data

    def getCurrent(self):
        if self.__rangeIdx == -1:
            raise Exception("Error: Current range has not been specified. Use setRange method.")
        
        countTot    = pow(2, self.AD_AD5421_BIT_RES) -1 
        minRange    = self.__ranges[self.__rangeIdx][0]
        maxRange    = self.__ranges[self.__rangeIdx][1]
        
        count = self.getDac()
        current = (maxRange - minRange) / countTot * count + minRange
        return current
    
    def setControl(self, wdTimeout=0b011, wdDisable=0, faultRbDisable=0, spiAlarmDisable=0, 
                   minCurrentEnable=0, adcInput=0, adcEnable=0, intRefDisable=0, vloopFaultEnable=0):
        
        data = (wdTimeout << 13) + (wdDisable << 12) + (faultRbDisable << 11) + (spiAlarmDisable << 10) + 
               (minCurrentEnable << 9) + (adcInput << 8) + (adcEnable << 7) + (intRefDisable << 6) + (vloopFaultEnable << 5)

        self.__sendWriteCmd(self.AD_AD5421_W_CONTR_REG, data) 

    def getControl(self):
        readback = self.__sendReadCmd(self.AD_AD5421_R_CONTR_REG, 0)
        data1, data0 = readback[1], readback[2]
        #return self.__mergeData(data1, data0)
        return [data1, data0]
        
    def setOffset(self, data):
        self.__sendWriteCmd(self.AD_AD5421_W_OFFS_REG, data)
            
    def getOffset(self):
        readback = self.__sendReadCmd(self.AD_AD5421_R_OFFS_REG, 0)
        data1, data0 = readback[1], readback[2]
        return self.__mergeData(data1, data0)
    
    def setGain(self, data):
        self.__sendWriteCmd(self.AD_AD5421_W_GAIN_REG, data)
            
    def getGain(self):
        readback = self.__sendReadCmd(self.AD_AD5421_R_GAIN_REG, 0)
        data1, data0 = readback[1], readback[2]
        return self.__mergeData(data1, data0)
    
    def getFaultReg(self):
        readback = self.__sendReadCmd(self.AD_AD5421_R_FAULT_REG, 0)
        data1, data0 = readback[1], readback[2]
        return [data1, data0]
        #return self.__mergeData(data1, data0)

    def loadDac(self):
        self.__sendWriteCmd(self.AD_AD5421_LOAD_DAC, 0) 
        
    def forceAlarmCurrent(self):
        self.__sendWriteCmd(self.AD_AD5421_FORCE_ALARM, 0)
        
    def reset(self):
        self.__sendWriteCmd(self.AD_AD5421_RESET, 0) 
        
        