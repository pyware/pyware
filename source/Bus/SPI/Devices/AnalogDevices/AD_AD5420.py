#!/usr/bin/python

from Bus.SPI.SpiDevice.GpioSpiDevice.GpioSpiDevice      import GpioSpiDevice
from Utils.Utils                                        import Utils
from struct import unpack, pack
import time

class AD_AD5420(GpioSpiDevice):
    
    # Registers
    AD_AD5420_NOP               = 0b00000000
    AD_AD5420_DATA_REG          = 0b00000001
    AD_AD5420_READB_REG         = 0b00000010
    AD_AD5420_CONTR_REG         = 0b01010101
    AD_AD5420_RESET_REG         = 0b01010110
    
    # Register addresses for read
    AD_AD5420_STATUS_ADDR       = 0b00
    AD_AD5420_DATA_ADDR         = 0b01
    AD_AD5420_CONTR_ADDR        = 0b10
    
    # Control options
    AD_AD5420_C_RANGE_4_20      = 0b101
    AD_AD5420_C_RANGE_0_20      = 0b110
    AD_AD5420_C_RANGE_0_24      = 0b111

    # Ranges (mA)
    AD_AD5420_MA_RANGE_4_20     = [4.0, 20.0]
    AD_AD5420_MA_RANGE_0_20     = [0.0, 20.0]
    AD_AD5420_MA_RANGE_0_24     = [0.0, 24.0]
    
    # Misc
    AD_AD5420_BIT_RES           = 16
    AD_AD5420_DATA_SIZE         = 2 # Num bytes

    def __init__(self, spi, cs):
        GpioSpiDevice.__init__(self, spi, cs)

        self.__maRanges = [self.AD_AD5420_MA_RANGE_4_20, self.AD_AD5420_MA_RANGE_0_20, self.AD_AD5420_MA_RANGE_0_24]
        self.__cRanges  = [self.AD_AD5420_C_RANGE_4_20, self.AD_AD5420_C_RANGE_0_20, self.AD_AD5420_C_RANGE_0_24]
        self.__rangeIdx = -1

    def __sendWriteCmd(self, reg, data):
        bytes = self.__splitData(data)
        #print "Write: %s" %bytes
        self.writeBlockReg(reg, bytes)
        
    def __sendReadCmd(self, regAddr):
        self.writeBlockReg(self.AD_AD5420_READB_REG, [0, regAddr]) # Send readback cmd to specified reg addr
        bytes = self.readBytes(self.AD_AD5420_DATA_SIZE + 1)
        #bytes = self.writeReadBytes([self.AD_AD5420_READB_REG, 0, regAddr], self.AD_AD5420_DATA_SIZE + 1)
        #print "Read: %s" %bytes
        return self.__mergeData(bytes[1:3]) # Ignore first byte
    
    def __mergeData(self, bytes):
        # Merge byte array into 1 uint
        data = 0
        b = bytes[::-1] # Reverse array
        for i in range(len(b)):
            data += (b[i] << (i*8))
        return data
    
    def __splitData(self, data):
        # Split uint16 (H) into 2 uint8 (B), byte order = little-endian (<)
        bytes = pack('<H', data)
        data0 = unpack('B', bytes[0])[0]
        data1 = unpack('B', bytes[1])[0]
        return [data1, data0] # MSB first

    def setup(self):
        # Set defaults
        self.reset()
        time.sleep(0.2)
        self.setControl(outEn=1)
        time.sleep(0.1)
        self.setDac(0)
        time.sleep(0.1)
        
    def cleanUp(self):
        self.setDac(0)
        
    def setRange(self, rangeIdx):
        if rangeIdx in range(len(self.__maRanges)):
            prevData = self.__sendReadCmd(self.AD_AD5420_CONTR_ADDR)
            
            outDisable = prevData & ~0x1000     # Unset output enable bit
            newData = prevData & ~0x7           # Unset prev range bits
            newData |= self.__cRanges[rangeIdx] # Set range bits with new value
            newData |= 0x1000                   # Set output enable bit

            self.__sendWriteCmd(self.AD_AD5420_CONTR_REG, outDisable)
            self.__sendWriteCmd(self.AD_AD5420_CONTR_REG, newData)
            self.__rangeIdx = rangeIdx
        else:
            raise Exception("Error: Range index %d given. Expected value 0-2." %rangeIdx)
    
    def getRange(self):
        return self.__maRanges[self.__rangeIdx]
    
    def setDac(self, data):
        self.__sendWriteCmd(self.AD_AD5420_DATA_REG, data)
            
    def getDac(self):
        return self.__sendReadCmd(self.AD_AD5420_DATA_ADDR)
    
    def setCurrent(self, current):
        current     = float(current)
        countTot    = pow(2, self.AD_AD5420_BIT_RES) - 1
        count       = 0
        
        if self.__rangeIdx == -1:
            raise Exception("Error: Current range has not been specified. Use setRange method.")
        
        minRange = self.__maRanges[self.__rangeIdx][0]
        maxRange = self.__maRanges[self.__rangeIdx][1]
        
        if minRange <= current <= maxRange:
            count = (current - minRange) / (maxRange - minRange) * countTot
        else:
        #    raise Exception("Error: Current %f given. Expected value %f - %f." %(current, minRange, maxRange))
            print "Warning: Current %f given. Expected value %f - %f." %(current, minRange, maxRange)

        data = int(round(count))        
        self.setDac(data)
        #print "Current = %f " %current
        #print "Count = %f " %count
        #print "Data = %d " %data

    def getCurrent(self):
        if self.__rangeIdx == -1:
            raise Exception("Error: Current range has not been specified. Use setRange method.")
        
        countTot    = pow(2, self.AD_AD5420_BIT_RES) -1 
        minRange    = self.__maRanges[self.__rangeIdx][0]
        maxRange    = self.__maRanges[self.__rangeIdx][1]
        
        count = self.getDac()
        current = (maxRange - minRange) / countTot * count + minRange
        return current
    
    def setControl(self, rExt=0, outEn=0, srClk=0, srStep=0, srEn=0, dcEn=0, range=0b101):
        data = (rExt<<13) + (outEn<<12) + (srClk<<8) + (srStep<<5) + (srEn<<4) + (dcEn<<3) + range
        self.__rangeIdx = range - 5 # gives range idx 0-2
        self.__sendWriteCmd(self.AD_AD5420_CONTR_REG, data)  

    def getControl(self):
        return self.__sendReadCmd(self.AD_AD5420_CONTR_ADDR)
        
    def getStatus(self):
        return self.__sendReadCmd(self.AD_AD5420_STATUS_ADDR)
        
    def reset(self):
        self.__sendWriteCmd(self.AD_AD5420_RESET_REG, 1)
        