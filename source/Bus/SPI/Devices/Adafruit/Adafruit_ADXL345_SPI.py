#!/usr/bin/python

from Bus.SPI.SpiEngine.SpiEngine    import SpiEngine
from Utils.Utils                    import Utils

class Adafruit_ADXL345_SPI(SpiEngine):

    # Minimal constants carried over from Arduino library
    
    # Registers
    ADXL345_REG_DEVID        = 0x00 # Device ID
    ADXL345_REG_DATA_FORMAT  = 0x31
    ADXL345_REG_DATAX0       = 0x32 # X-axis data 0 (6 bytes for X/Y/Z)
    ADXL345_REG_DATA_RATE    = 0x2C 
    ADXL345_REG_POWER_CTL    = 0x2D # Power-saving features control
    
    # Datarate values
    ADXL345_DATARATE_0_10_HZ = 0x00
    ADXL345_DATARATE_0_20_HZ = 0x01
    ADXL345_DATARATE_0_39_HZ = 0x02
    ADXL345_DATARATE_0_78_HZ = 0x03
    ADXL345_DATARATE_1_56_HZ = 0x04
    ADXL345_DATARATE_3_13_HZ = 0x05
    ADXL345_DATARATE_6_25HZ  = 0x06
    ADXL345_DATARATE_12_5_HZ = 0x07
    ADXL345_DATARATE_25_HZ   = 0x08
    ADXL345_DATARATE_50_HZ   = 0x09
    ADXL345_DATARATE_100_HZ  = 0x0A # (default)
    ADXL345_DATARATE_200_HZ  = 0x0B
    ADXL345_DATARATE_400_HZ  = 0x0C
    ADXL345_DATARATE_800_HZ  = 0x0D
    ADXL345_DATARATE_1600_HZ = 0x0E
    ADXL345_DATARATE_3200_HZ = 0x0F
    
    # Range values
    ADXL345_RANGE_2_G        = 0x00 # +/-  2g (default)
    ADXL345_RANGE_4_G        = 0x01 # +/-  4g
    ADXL345_RANGE_8_G        = 0x02 # +/-  8g
    ADXL345_RANGE_16_G       = 0x03 # +/- 16g
    
    # FIFO settings
    ADXL345_FIFO_BYPASS      = 0x00
    ADXL345_FIFO_FIFO        = 0x40
    ADXL345_FIFO_STREAM      = 0x80
    ADXL345_FIFO_TRIGGER     = 0xC0

    def __init__(self, bus, device):
        SpiEngine.__init__(self, bus, device)
        
        self.setMode(3)
        
        id = self.readBlockReg(self.ADXL345_REG_DEVID, 1)[0]
        if id == 0xe5:
            # Enable and config the accelerometer
            self.writeBlockReg(self.ADXL345_REG_POWER_CTL, [0x08]) 
            self.writeBlockReg(self.ADXL345_REG_DATA_RATE, [self.ADXL345_DATARATE_100_HZ])     
            self.writeBlockReg(self.ADXL345_REG_DATA_FORMAT, [0x10])
        else:
            raise Exception("Error: Device ID %s read, should be 0xE5." %str(hex(id)).upper())
    
    def readBlockReg(self, reg, len):
        # Add FIFO setting to register command
        return super(Adafruit_ADXL345_SPI, self).readBlockReg(reg | self.ADXL345_FIFO_TRIGGER, len)
     
    """
    def setRange(self, range):
        # Read the data format register to preserve bits.  Update the data
        # rate, make sure that the FULL-RES bit is enabled for range scaling
        format = ((self.accel.readU8(self.ADXL345_REG_DATA_FORMAT) & ~0x0F) |
          range | 0x08)
        # Write the register back to the IC
        seld.accel.write8(self.ADXL345_REG_DATA_FORMAT, format)


    def getRange(self):
        return self.accel.readU8(self.ADXL345_REG_DATA_FORMAT) & 0x03


    def setDataRate(self, dataRate):
        # Note: The LOW_POWER bits are currently ignored,
        # we always keep the device in 'normal' mode
        self.accel.write8(self.ADXL345_REG_BW_RATE, dataRate & 0x0F)


    def getDataRate(self):
        return self.accel.readU8(self.ADXL345_REG_BW_RATE) & 0x0F
    """
    
    # Read the accelerometer
    def read(self):
        raw = self.readBlockReg(self.ADXL345_REG_DATAX0, 6)
        #print raw
        res = []
        for i in range(0, 6, 2):
            g = raw[i] | (raw[i+1] << 8)
            if g > 32767: g -= 65536
            res.append(g)
        return res


# Simple example prints accelerometer data once per second:
if __name__ == '__main__':

    from time import sleep

    accel = Adafruit_ADXL345(0,0)

    print '[Accelerometer X, Y, Z]'
    while True:
        print accel.read()
        sleep(1) # Output is fun to watch if this is commented out
