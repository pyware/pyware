#!/usr/bin/python

from Bus.SPI.Devices.Microchip.MC_MCP3903   import MC_MCP3903
from Buffer.RingBuffer.RingBuffer           import RingBuffer
#import numpy # If using ...Median2 class

# Subclass to read filtered data using deque buffer. 
# ADC introduces random spurious data for some reason.
class MC_MCP3903_Median(MC_MCP3903):
    
    def __init__(self, spi, cs):
        MC_MCP3903.__init__(self, spi, cs)
        
        bufferSize = 3
        self.__voltageBuffer = []
        
        for ch in range(self.MC_MCP3903_NUM_CH):
            self.__voltageBuffer.append(RingBuffer(bufferSize))
        
    def getVoltageCont(self):
        volts = super(MC_MCP3903_Median, self).getVoltageCont()
        voltsMedian = []

        for ch in range(self.MC_MCP3903_NUM_CH):
            self.__voltageBuffer[ch].append(volts[ch])
            voltsMedian.append(self.__voltageBuffer[ch].median)

        return voltsMedian
            
    def getVoltage(self, ch):
        volt = super(MC_MCP3903_Median, self).getVoltage(ch)
        self.__voltageBuffer[ch].append(volt)
        
        return self.__voltageBuffer[ch].median

        
# Subclass to MC_MCP3903 to read filtered data using numpy. 
# ADC introduces random spurious data for some reason.
# Using numpy requires more resources
class MC_MCP3903_Median2(MC_MCP3903):
   
    def __init__(self, spi, cs):
        MC_MCP3903.__init__(self, spi, cs)
        
        windowSize = 3
        self.__voltageWindow        =  numpy.zeros((windowSize, self.MC_MCP3903_NUM_CH))
        self.__channelWindowIndex   =  numpy.zeros(self.MC_MCP3903_NUM_CH)
        self.__windowIndex          = 0
        
    def getVoltageCont(self):
        volts = super(MC_MCP3903_Median2, self).getVoltageCont()
        
        self.__voltageWindow[self.__windowIndex,:] = volts
        self.__windowIndex = self.__windowIndex + 1
        self.__windowIndex = self.__windowIndex % self.__voltageWindow[:,0].size

        for ch in range(self.MC_MCP3903_NUM_CH):
            volts[ch] = numpy.median(self.__voltageWindow[:,ch])            

        return volts
            
    def getVoltage(self, ch):
        volt = super(MC_MCP3903_Median2, self).getVoltage(ch)

        self.__voltageWindow[self.__channelWindowIndex[ch],ch] = volt
        self.__channelWindowIndex[ch] = self.__channelWindowIndex[ch] + 1
        self.__channelWindowIndex[ch] = self.__channelWindowIndex[ch] % self.__voltageWindow[:,0].size
        
        return numpy.median(self.__voltageWindow[:,ch])    

        