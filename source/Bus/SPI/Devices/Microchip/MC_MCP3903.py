#!/usr/bin/python

from Bus.SPI.SpiDevice.GpioSpiDevice.GpioSpiDevice      import GpioSpiDevice
from Utils.Utils                                        import Utils
from struct import unpack, pack
import time

class MC_MCP3903(GpioSpiDevice):

    # Device Address
    MC_MCP3903_DEV_ADDR     = 0x01 # Default
    
    # Registers
    MC_MCP3903_REG_CH0      = 0x00
    MC_MCP3903_REG_CH1      = 0x01
    MC_MCP3903_REG_CH2      = 0x02
    MC_MCP3903_REG_CH3      = 0x03
    MC_MCP3903_REG_CH4      = 0x04
    MC_MCP3903_REG_CH5      = 0x05
    
    MC_MCP3903_REG_MOD      = 0x06
    MC_MCP3903_REG_PHASE    = 0x07
    MC_MCP3903_REG_GAIN     = 0x08
    MC_MCP3903_REG_STATUS   = 0x09
    MC_MCP3903_REG_CONFIG   = 0x0A
    
    # Read/Write
    MC_MCP3903_WRITE        = 0x00
    MC_MCP3903_READ         = 0x01
    
    # Gain
    MC_MCP3903_GAIN_1       = 0x00 # +-0.5 V
    MC_MCP3903_GAIN_2       = 0x01 # +-0.25 V
    MC_MCP3903_GAIN_4       = 0x02 # +-0.125 V
    MC_MCP3903_GAIN_8       = 0x03 # +-0.0625 V
    MC_MCP3903_GAIN_16      = 0x04 # +-0.03125 V
    MC_MCP3903_GAIN_32      = 0x05 # +-0.015625 V
    
    # OSR
    MC_MCP3903_OSR_32       = 0x00
    MC_MCP3903_OSR_64       = 0x01 # Default
    MC_MCP3903_OSR_128      = 0x02
    MC_MCP3903_OSR_256      = 0x03
    
    # Misc
    MC_MCP3903_DATA_SIZE    = 3 # Num bytes
    MC_MCP3903_NUM_CH       = 6
    MC_MCP3903_INT_REF_VOLT = 2.35
    MC_MCP3903_BIT_RES_24   = 24
    MC_MCP3903_BIT_RES_16   = 16
    
    def __init__(self, spi, cs):
        GpioSpiDevice.__init__(self, spi, cs)
        
        self.__refVolt  = self.MC_MCP3903_INT_REF_VOLT
        self.__bitRes   = self.MC_MCP3903_BIT_RES_16
        self.__highRes  = False
        self.__gains    = []
        
        for ch in range(self.MC_MCP3903_NUM_CH):
            self.__gains.append(-1) # Set invalid value by default
        
    def __formatCmd(self, reg, rw):
        if self.MC_MCP3903_REG_CH0 <= reg <= self.MC_MCP3903_REG_CONFIG:
            cmd = (self.MC_MCP3903_DEV_ADDR << 6) + (reg << 1) + rw
            #print "CMD: %s" %cmd
            return cmd
        else:
            raise Exception("Error: Register %d given. Expected value 0-10." %reg)
        
    def __sendReadCmd(self, reg):
        cmd = self.__formatCmd(reg, self.MC_MCP3903_READ)
        bytes = self.readBlockReg(cmd, self.MC_MCP3903_DATA_SIZE) # Read 3 bytes from specified register
        #print "Read: %s" %bytes
        return self.__mergeData(bytes)
        
    def __sendContReadCmd(self, reg, numBytes):
        cmd = self.__formatCmd(reg, self.MC_MCP3903_READ)
        bytes = self.readBlockReg(cmd, numBytes)
        #print "Read: %s" %bytes
        return bytes
    
    def __sendWriteCmd(self, reg, data):
        cmd = self.__formatCmd(reg, self.MC_MCP3903_WRITE)
        bytes = self.__splitData(data)
        #print "Write: %s" %bytes
        self.writeBlockReg(cmd, bytes) # Write data bytes to specified register
            
    def __mergeData(self, bytes):
        # Merge byte array into 1 uint
        data = 0
        b = bytes[::-1] # Reverse array
        for i in range(len(b)):
            data += (b[i] << (i*8))
        return data
        
    def __splitData(self, data):
        bytes = []
        # Split uint32 (I) into 4 uint8 bytes, byte order = little-endian (<)
        b = pack('<I', data)
        for i in range(self.MC_MCP3903_DATA_SIZE): # Ignore last byte (24 bits total)
            bytes.append(unpack('B', b[i])[0])
        return bytes[::-1] # Reverse array
    
    def __calcVoltage(self, ch, data):
        # For single channel read the voltage calculation is always based on 24 bit data, regardless of bit resolution setting
        if self.__gains[ch] >= 0:
            # Pack data as uint32, then unpack as int32 for 2s-comp coding
            # Shift 8 bits to convert from/to 24 bit data
            b = pack('<I', (data << 8))
            count = (unpack('<i', b)[0] >> 8)
            volt = (count * self.__refVolt) / (pow(2, self.MC_MCP3903_BIT_RES_24-1) * (1 << self.__gains[ch]) * 3)
            return volt
        else:
            raise Exception("Error: Gain has not been set. Use setup or setGain method.")
            
    def __calcVoltageCont(self, ch, data):
        # For continuous read the bit resolution setting must be included when calculating voltage
        if self.__gains[ch] >= 0:
            if self.__highRes:
                # Pack data as uint32, then unpack as int32 for 2s-comp coding
                # Shift 8 bits to convert from/to 24 bit data
                b = pack('<I', (data << 8))
                count = (unpack('<i', b)[0] >> 8)
            else:
                # Pack data as uint16, then unpack as int16 for 2s-comp coding
                b = pack('<H', (data))
                count = unpack('<h', b)[0]
            volt = (count * self.__refVolt) / (pow(2, self.__bitRes-1) * (1 << self.__gains[ch]) * 3)
            return volt
        else:
            raise Exception("Error: Gain has not been set. Use setup or setGain method.")
    
    def setup(self, osr=1, highRes=False):
        self.__highRes = highRes
        status  = 0
        config1 = 0xfc0fd0                  # Set reset mode, rest default
        config2 = 0x000fc0 | (osr << 4)     # Set reset mode off and OSR, rest default
        
        if self.MC_MCP3903_OSR_32 <= osr <= self.MC_MCP3903_OSR_256:
            self.setConfigRaw(config1)
            time.sleep(0.1)
            self.setConfigRaw(config2)
            time.sleep(0.1)
        else:
            raise Exception("Error: OSR %d given. Expected value 0-3." %osr)

        if highRes:
            status = 0x9FC000               # Set 24 bit mode, rest default
            self.__bitRes = self.MC_MCP3903_BIT_RES_24
        else:
            status = 0x804000               # Set 16 bit mode, rest default
            self.__bitRes = self.MC_MCP3903_BIT_RES_16

        self.setStatusRaw(status)
        gain = self.MC_MCP3903_GAIN_1       # Gain = 1 by default
        self.setGainAll(self.MC_MCP3903_GAIN_1, boost=0)
        
    def getRef(self):
        return self.__refVolt
        
    def setRef(self, volt):
        self.__refVolt = volt
    
    def getAdc(self, ch):
        if self.MC_MCP3903_REG_CH0 <= ch <= self.MC_MCP3903_REG_CH5:
            return self.__sendReadCmd(ch)
        else:
            raise Exception("Error: Channel %d given. Expected value 0-5." %ch)

    def getAdcAll(self):
        dataArr = []
        for ch in range(self.MC_MCP3903_NUM_CH):
            dataArr.append(self.getAdc(ch))
        return dataArr
        
    def getVoltageCont(self):
        # Continuous read from all channels
        numCh = self.MC_MCP3903_NUM_CH
        volts = []
        dataSize = 2
        
        if self.__highRes:
            dataSize = 3
        bytesAll = self.__sendContReadCmd(self.MC_MCP3903_REG_CH0, numCh*dataSize)
        
        for i in range(numCh):
            data = self.__mergeData(bytesAll[(i*dataSize):(i*dataSize+dataSize)])
            volts.append(self.__calcVoltageCont(i, data))           

        return volts
            
    def getVoltage(self, ch):
        data = self.getAdc(ch)
        #print "ADC %d: %s" %(ch, data)
        return self.__calcVoltage(ch, data)
        
    def getVoltageDebug(self, ch):
        data = self.getAdc(ch)
        #print "ADC %d: %s" %(ch, data)
        volt = self.__calcVoltage(ch, data)
        return volt, data
        #return self.__calcVoltage(ch, data)
        
    def getGain(self, ch):
        data    = self.__sendReadCmd(self.MC_MCP3903_REG_GAIN)
        bitStr  = str('{:024b}'.format(data))
        #print "Get Gain: %s" %bitStr
        chIdx   = ch*4
        
        # Boost bit is for every other channel MSB or LSB
        if ch in [0,2,4]:
            chGain  = int(bitStr[chIdx:(chIdx+3)],2)
            chBoost = int(bitStr[(chIdx+3):(chIdx+4)],2)
        elif ch in [1,3,5]:
            chBoost = int(bitStr[chIdx:(chIdx+1)],2)
            chGain  = int(bitStr[(chIdx+1):(chIdx+4)],2)
        else:
            raise Exception("Error: Channel %d given. Expected value 0-5." %ch)
        
        return chGain, chBoost
    
    def setGain(self, ch, gain, boost=0):
        chGain  = 0
        chIdx   = ch*4
        
        # Boost bit is for every other channel MSB or LSB
        if ch in [0,2,4]:
            chGain = (boost << 3) + gain
        elif ch in [1,3,5]:
            chGain = (gain << 1) + boost
        else:
            raise Exception("Error: Channel %d given. Expected value 0-5." %ch)
        
        if self.MC_MCP3903_GAIN_1 <= gain <= self.MC_MCP3903_GAIN_32:
            # Read existing gain settings for all channels
            prevGain = self.__sendReadCmd(self.MC_MCP3903_REG_GAIN)
            # Update gain for selected channel only and leave the rest unchanged (~ = bit invert)
            newGain = prevGain & ~(0xf << chIdx)
            newGain = newGain | (chGain << chIdx)
            
            self.__sendWriteCmd(self.MC_MCP3903_REG_GAIN, newGain)
            self.__gains[ch] = gain
        else:
            raise Exception("Error: Gain %d given. Expected value 0-5." %gain)
        
    def setGainAll(self, gain, boost=0):
        newGain = 0
        if self.MC_MCP3903_GAIN_1 <= gain <= self.MC_MCP3903_GAIN_32:
            # Set same gain and boost for all channels
            for ch in range(self.MC_MCP3903_NUM_CH):
                self.__gains[ch] = gain
                chIdx   = ch*4
                # Boost bit is for every other channel MSB or LSB
                if ch in [0,2,4]:
                    chGain = (boost << 3) + gain
                elif ch in [1,3,5]:
                    chGain = (gain << 1) + boost
                newGain |= (chGain << chIdx)

            self.setGainRaw(newGain)
        else:
            raise Exception("Error: Gain %d given. Expected value 0-5." %gain)

    def getGainRaw(self):
        data = self.__sendReadCmd(self.MC_MCP3903_REG_GAIN)
        return data
        
    def setGainRaw(self, gain):
        self.__sendWriteCmd(self.MC_MCP3903_REG_GAIN, gain)
        
    def getModRaw(self): # Read-only
        data = self.__sendReadCmd(self.MC_MCP3903_REG_MOD)
        return data
        # TBD: Split data into channels

    def getPhaseRaw(self):
        data = self.__sendReadCmd(self.MC_MCP3903_REG_PHASE)
        return data
        # TBD: Split data into channels
        
    def setPhaseRaw(self, phase):
        self.__sendWriteCmd(self.MC_MCP3903_REG_PHASE, phase)
        # TBD: Split data into channels
    
    def getStatusRaw(self):
        data = self.__sendReadCmd(self.MC_MCP3903_REG_STATUS)
        #print "Get Status: %s" %data
        return data
        
    def setStatusRaw(self, status):
        self.__sendWriteCmd(self.MC_MCP3903_REG_STATUS, status)
        #print "Set Status: %s" %status
        
    def getConfigRaw(self):
        data = self.__sendReadCmd(self.MC_MCP3903_REG_CONFIG)
        #print "Get Config: %s" %data
        return data
        
    def setConfigRaw(self, config):
        self.__sendWriteCmd(self.MC_MCP3903_REG_CONFIG, config)
        #print "Set Config: %s" %config
