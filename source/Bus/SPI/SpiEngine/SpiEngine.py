#!/usr/bin/python
 
from Adafruit_BBIO.SPI import SPI
#import spidev

class SpiEngine(object):
    def __init__(self, bus, device):
        
        self.__spi = SPI(bus, device)
        
        # When using spidev
        #self.__spi = spidev.SpiDev()
        #self.__spi.open(bus,device)
        
        """
        # Read SPI bus settings
        print self.__spi.msh
        print self.__spi.mode
        print self.__spi.bpw
        print self.__spi.threewire
        print self.__spi.cshigh
        print self.__spi.lsbfirst
        """
        
    def setSpeed(self, speed):
        self.__spi.msh = speed
        
        # When using spidev
        #self.__spi.max_speed_hz = speed
        
    def getSpeed(self):
        return self.__spi.msh
        
        # When using spidev
        #return self.__spi.max_speed_hz
        
    def setMode(self, mode):
        self.__spi.mode = mode
        
    def getMode(self):
        return self.__spi.mode
        
    def close(self):
        self.__spi.close()

    def readBytes(self, len):
        return self.__spi.readbytes(len)
        #return self.__spi.xfer2([0]*len, 0, 0)
    
    def writeBytes(self, data):
        self.__spi.writebytes(data)
    
    def xferBytes(self, data):
        # Use xfer2 to hold cs low for all bytes.
        return self.__spi.xfer2(data)
        
    def readBlockReg(self, reg, len):
        # Fill up with 0s after reg address for the amount of bytes to read.
        # Ignore first read byte.
        return self.xferBytes([reg]+[0]*len)[1:]
        
    def writeBlockReg(self, reg, data):
        data.insert(0,reg)
        self.xferBytes(data)
        
    def read(self):
        # Should be overridden
        raise NotImplementedError

