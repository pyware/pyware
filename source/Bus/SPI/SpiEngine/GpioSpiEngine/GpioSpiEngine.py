#!/usr/bin/python
 
import Adafruit_BBIO.GPIO           as GPIO
from Bus.SPI.SpiEngine.SpiEngine    import SpiEngine
from threading                      import Lock

class GpioSpiEngine(SpiEngine):
    def __init__(self, cs_pins):
        SpiEngine.__init__(self, bus=1, device=0)
        # Two buses available; 1 and 2
        # Use GPIOs as CS since BB lacks enough dedicated CS pins.
        # Input cs_pins are represented by an array of strings containing the pin names, eg ["P9_27", "P9_42", ...].
        self.__csPins   = cs_pins 
        #self.__cs       = 0
        #self.__mode     = self.getMode()
        self.__mutex    = Lock()
        
        for pin in self.__csPins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.HIGH) # Set all CS high by default
    
    def doOperation(self, dataR, mode, cs, op, dataW=[], lenR=0):
        # Common method with mutex lock to add thread safe behaviour
        # Use dataR list (created by caller) in attr and append data instead of using return
        # This is to avoid race conditions if lock is released before returning data

        try:
            self.__mutex.acquire()
            GPIO.output(self.__csPins[cs], GPIO.LOW)
            self.setMode(mode)
            if      op == 0:
                dataR += self.readBytes(lenR)
            elif    op == 1:
                self.writeBytes(dataW)
            elif    op == 2:
                dataR += self.xferBytes(dataW)
            elif    op == 3:
                self.writeBytes(dataW)
                dataR += self.readBytes(lenR)
            elif    op == 4:
                self.writeBytes(dataW)
                dataR += self.xferBytes([0]*lenR)
            else:
                raise Exception("Error: Operation %d not valid. Select a value between 0 - 3." %op)        

        finally:
            GPIO.output(self.__csPins[cs], GPIO.HIGH)
            self.__mutex.release()

    def checkCs(self, cs):
        # Check that CS is valid
        if 0 <= cs <= len(self.__csPins):
            pass
        else:
            raise Exception("Error: CS %d not valid. Select a value between %d and %d." %(cs, 0, len(self.__csPins)))

    """
    def selectCs(self, cs):
        self.checkCs(cs)
        self.__cs = cs

    def selectMode(self, mode):
        self.setMode(mode)
    """

    def close(self):
        #self.__mutex.release()
        GPIO.cleanup()
        super(GpioSpiEngine, self).close()
