#!/usr/bin/env python

import io
import fcntl
I2C_SLAVE = 0x0703

# Not working!
# Attempt to run I2C without smbus
class Raw_I2C(object):

    def __init__(self, address, bus=0):

        self.fr = io.open("/dev/i2c-"+str(bus), "rb", buffering=0)
        self.fw = io.open("/dev/i2c-"+str(bus), "wb", buffering=0)

        # Set device address
        fcntl.ioctl(self.fr, I2C_SLAVE, address)
        fcntl.ioctl(self.fw, I2C_SLAVE, address)

    def write(self, bytes):
        self.fw.write(bytes)

    def read(self, bytes):
        return self.fr.read(bytes)

    def close(self):
        self.fw.close()
        self.fr.close()