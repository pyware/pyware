#!/usr/bin/python

from Adafruit_I2C import Adafruit_I2C

class TI_TMP112(object):

    TMP112_ADDRESS        = 0x49

    def __init__(self, busnum=-1, debug=False):
        
        self.temp = Adafruit_I2C(self.TMP112_ADDRESS, busnum, debug)
    
    def setup(self):
        pass
            
    def cleanUp(self):
        pass
        
    def read(self):
        
        print self.temp.readU8(0)
        print self.temp.readByte()
        print self.temp.readByte()
        #count = self.temp.readU16(0) #>> 4
        #print str('{:016b}'.format(count))  
        #temp = count * 0.0625
        return 0 #temp

# Simple example prints accelerometer data once per second:
if __name__ == '__main__':

    from time import sleep

    temp = TI_TMP112()

    while True:
        #print "Temp = %s deg C" %temp.read()
        print temp.read()
        sleep(1)
