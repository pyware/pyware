#!/usr/bin/python

from Adafruit_I2C import Adafruit_I2C
from struct import unpack, pack

# Baseclass handling both ports
class TI_TCA6416A(object):

    TCA6416A_ADDRESS        = 0x21
    
    TCA6416A_REG_IN_0       = 0x00
    TCA6416A_REG_IN_1       = 0x01
    TCA6416A_REG_OUT_0      = 0x02
    TCA6416A_REG_OUT_1      = 0x03
    TCA6416A_REG_POL_INV_0  = 0x04
    TCA6416A_REG_POL_INV_1  = 0x05
    TCA6416A_REG_CONF_0     = 0x06
    TCA6416A_REG_CONF_1     = 0x07
    
    TCA6416A_CONF_INPUT     = 1
    TCA6416A_CONF_OUTPUT    = 0

    def __init__(self, busnum=-1, debug=False):

        self.exp = Adafruit_I2C(self.TCA6416A_ADDRESS, busnum, debug)
    
    def _numToBinList(self, num):
        numCh = 8
        binList = [0]*numCh
        i = 0
        while num != 0:
            bit = num % 2
            binList[i] = bit
            num = num / 2
            i += 1
        return binList
        
    def _binListToNum(self, list):
        num = 0
        for i in range(len(list)):
            num += (list[i] << i)
        return num
    
    def setup(self):
        # Configure all ports as inputs by default
        self.setConfig(0x00, 0x00)
        
        # Read configuration and validate
        port0, port1 = self.getConfig()
        conf = port0 + port1
        if conf != 0x00:
            raise Exception("Error: Configuration 0x%02X read, should be 0x00." %conf)
            
    def cleanUp(self):
        # Configure all ports as inputs to be safe
        self.setConfig(0x00, 0x00)
        
    def readInputs(self):
        port0 = self.exp.readU8(self.TCA6416A_REG_IN_0)
        port1 = self.exp.readU8(self.TCA6416A_REG_IN_1)
        return port0, port1
    
    def readOutputs(self):
        port0 = self.exp.readU8(self.TCA6416A_REG_OUT_0)
        port1 = self.exp.readU8(self.TCA6416A_REG_OUT_1)
        return port0, port1
        
    def writeOutputs(self, port0, port1):
        self.exp.write8(self.TCA6416A_REG_OUT_0, port0)
        self.exp.write8(self.TCA6416A_REG_OUT_1, port1)
    
    def getPolInv(self):
        port0 = self.exp.readU8(self.TCA6416A_REG_POL_INV_0)
        port1 = self.exp.readU8(self.TCA6416A_REG_POL_INV_1)
        return port0, port1
        
    def setPolInv(self, port0, port1):
        self.exp.write8(self.TCA6416A_REG_POL_INV_0, port0)
        self.exp.write8(self.TCA6416A_REG_POL_INV_1, port1)
        
    def getConfig(self):
        port0 = self.exp.readU8(self.TCA6416A_REG_CONF_0)
        port1 = self.exp.readU8(self.TCA6416A_REG_CONF_1)
        return port0, port1
        
    def setConfig(self, port0, port1):
        self.exp.write8(self.TCA6416A_REG_CONF_0, port0)
        self.exp.write8(self.TCA6416A_REG_CONF_1, port1)
    
    def readList(self):
        list = []
        ports = self.readInputs()
        list.extend(self._numToBinList(ports[0]))
        list.extend(self._numToBinList(ports[1]))
        return list
        
    def writeList(self, list):
        port0 = self._binListToNum(list[0:8])
        port1 = self._binListToNum(list[8:16])
        self.writeOutputs(port0, port1)
        
    def read(self):
        ports = self.readInputs()
        # Bit shift port1 8 bits to merge the inputs into 16 bit representation.
        inputs = (ports[1] << 8) + ports[0]
        return inputs
    
    def write(self, outputs):
        if 0 <= outputs < pow(2,16):
            # Split uint16 (H) into 2 uint8 (B), byte order = little-endian (<)
            bytes = pack('<H', outputs)
            port0 = unpack('B', bytes[0])[0]
            port1 = unpack('B', bytes[1])[0]
            self.writeOutputs(port0, port1)
        else:
            raise Exception("Error: Output value %d given. Expected value 0-65535 (2^16 - 1)." %outputs)


# Subclass handling only 1 of the ports (SP = single port)
class TI_TCA6416A_SP(TI_TCA6416A):
    def __init__(self, busnum=-1, port=0):
        TI_TCA6416A.__init__(self, busnum, False)
        
        if port in [0,1]:
            self.__port = port
        else:
            raise Exception("Error: Port %d given. Expected value 0 or 1." %port)
        
    def setup(self):
        # Configure all ports as inputs by default
        self.setConfig(0x00)
        
        # Read configuration and validate
        conf = self.getConfig()
        #if conf != 0x00:
        #    raise Exception("Error: Configuration 0x%02X read, should be 0x00." %conf)
    
    def cleanUp(self):
        # Configure all ports as inputs to be safe
        self.setConfig(0x00)
        
    def readInputs(self):
        return self.exp.readU8(self.TCA6416A_REG_IN_0 + self.__port)
    
    def readOutputs(self):
        return self.exp.readU8(self.TCA6416A_REG_OUT_0 + self.__port)
        
    def writeOutputs(self, port):
        self.exp.write8(self.TCA6416A_REG_OUT_0 + self.__port, port)
    
    def getPolInv(self):
        return self.exp.readU8(self.TCA6416A_REG_POL_INV_0 + self.__port)
       
    def setPolInv(self, port):
        self.exp.write8(self.TCA6416A_REG_POL_INV_0 + self.__port, port)
        
    def getConfig(self):
        return self.exp.readU8(self.TCA6416A_REG_CONF_0 + self.__port)
        
    def setConfig(self, port):
        self.exp.write8(self.TCA6416A_REG_CONF_0 + self.__port, port)
    
    def readList(self):
        return self._numToBinList(self.readInputs())
        
    def writeList(self, list):
        self.writeOutputs(self._binListToNum(list))
        
    def read(self):
        return self.readInputs()
    
    def write(self, outputs):
        if 0 <= outputs < pow(2,8):
            self.writeOutputs(outputs)
        else:
            raise Exception("Error: Output value %d given. Expected value 0-255 (2^8 - 1)." %outputs)