#!/usr/bin/python

#from Adafruit_I2C import Adafruit_I2C
from Bus.I2C.Raw_I2C import Raw_I2C
from struct import unpack, pack

class Catalyst_CAT24C256(Raw_I2C):

    # Fixed 4-bit address (rest is determined by A0-A2 pins)
    CAT24C256_FIXED_ADDRESS       = 0b1010

    def __init__(self, address, bus):
        
        address = (self.CAT24C256_FIXED_ADDRESS << 3) + address
        print '{:08b}'.format(address)
        print '0x%02X' %address
        #self.eeprom = Adafruit_I2C(self.__address, busnum, debug)
        
        I2C_Base.__init__(self, address, bus)
