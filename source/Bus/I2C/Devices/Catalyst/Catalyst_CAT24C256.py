#!/usr/bin/python

#from Adafruit_I2C import Adafruit_I2C
from Bus.I2C.Adafruit_I2C   import Adafruit_I2C
from struct import unpack, pack

class Catalyst_CAT24C256(Adafruit_I2C):

    # Base address (rest is determined by A0-A2 pins)
    CAT24C256_BASE_ADDRESS       = 0b1010

    def __init__(self, address, busnum=-1, debug=False):
        
        self.__address = (self.CAT24C256_BASE_ADDRESS << 3) + address
        print '{:08b}'.format(self.__address)
        print '0x%02X' %self.__address
        self.eeprom = Adafruit_I2C(self.__address, busnum, debug)

    def readData(self, byteAddr):
        return self.eeprom.readU8(byteAddr)
        #return self.eeprom.readU16(byteAddr)
        
    def read(self):
        return self.eeprom.readByte()
    
    def writeToAddr(self, addr, data):
        d = []
        d.append(addr[0])
        d.extend(data)
        self.eeprom.writeList(addr[1], d)
        
    def readFromAddr(self, addr, len):
        return self.eeprom.readList(addr, len)
        

# Simple example
if __name__ == '__main__':

    from time import sleep
    
    eeprom = Catalyst_CAT24C256(0, -1, 1)

    print '[Inputs]'
    while True:
        print '{:08b}'.format(eeprom.read())
        #print eeprom.readData(0)
        sleep(1)