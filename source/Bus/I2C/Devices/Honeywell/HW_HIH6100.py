from smbus import SMBus
import time

bus = SMBus(1)
address = 0x27

while 1:
    data = bus.read_i2c_block_data(address, 0x00, 4)
    status = (data[0] & 0b11000000) >> 6
    hum_msb = data[0] & 0b00111111
    hum_lsb = data[1]
    temp_msb = data[2]
    temp_lsb = (data[3] & 0b11111100) >> 2
    hum_raw = (hum_msb << 8) + hum_lsb
    temp_raw = (temp_msb << 6) + temp_lsb

    hum = float(hum_raw) / (pow(2, 14)-2) * 100
    temp = float(temp_raw) / (pow(2, 14)-2) * 165 - 40

    print data
    #print data[1]
    print status
    print hum_raw
    print temp_raw
    print hum
    print temp
    time.sleep(1)
