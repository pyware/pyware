#!/usr/bin/python

import imudev

from struct import unpack, pack
import time, math

class SensorType:
    # Use class as type-def
    # Set start index for data content
    Mag     = 0
    Acc     = 6
    Gyro    = 12
    Temp    = 18
    # Set order of returned data
    SensorTypes = [Acc, Gyro, Mag, Temp]
    
class BbbImu(object):

    # Misc const
    MISC_ACCEL_SCALES   = [2, 4, 8, 16]             # g
    MISC_GYRO_SCALES    = [250, 500, 1000, 2000]    # deg/s
    MISC_MAG_SCALES     = [4912]                    # uT

    def __init__(self, numSensors=1):
        # Init scales (must match setup in FW)
        self.__accelScale   = self.MISC_ACCEL_SCALES[0]*9.80665     # m/s2
        self.__gyroScale    = self.MISC_GYRO_SCALES[0]*3.14159/180  # rad/s 
        self.__magScale     = self.MISC_MAG_SCALES[0]               # uT
        
        self.__numChannels  = 10 # 3*accel, 3*gyro, 3*mag, 1*temp
        self.__numSensors   = numSensors # 1 to 4 sensors available
        self.__imu          = imudev.ImuDev()
            
    def __mergeData(self, lsb, msb):
        # Merge 2 bytes into 1 int16
        uint16 = pack('<H', (msb<<8) + lsb)
        int16 = unpack('h', uint16)[0]
        return int16
    
    def __scaleData(self, rawData, scale):
        # Scale raw int16 value to actual value
        return float(rawData) / pow(2, 16-1) * scale
        
    def __getXyz(self, data, sensNum, sensType, scale):
        i = sensType
        # Read 6 bytes for XH, XL, YH, YL, ZH, ZL
        x = self.__scaleData(self.__mergeData(data[0+i], data[1+i]), scale)
        y = self.__scaleData(self.__mergeData(data[2+i], data[3+i]), scale)
        z = self.__scaleData(self.__mergeData(data[4+i], data[5+i]), scale)
        
        # Sensors are rotated compared to x,y marks on PCB
        # Sensor 0-1 are rotated 180 deg compared to sensor 2-3
        # Mag is rotated compared to Acc and Gyro, see spec
        if sensNum in [0,1]:
            if sensType == SensorType.Mag:
                return [x,-y,-z]
            else:
                return [y,-x,z]
        elif sensNum in [2,3]:
            if sensType == SensorType.Mag:
                return [-x,y,-z]
            else:
                return [-y,x,z]
        else:
                return [x,y,z]
            
    def __getTemp(self, data):
        # TEMP_degC: ((TEMP_OUT - RoomTemp_Offset)/Temp_Sensitivity) + 21degC
        # 2 bytes for temperature H and L
        return ((self.__mergeData(data[SensorType.Temp], data[SensorType.Temp+1]) - 0)/333.87 + 21)
    
    def __calcMean(self, data):
        return float(sum(data)) / max(len(data), 1)
    
    def setup(self):
        #self.__imu.open() # open already done in constructor
        self.__imu.write([0x01]) # 1 = start sampling
        
    def cleanUp(self):
        self.__imu.write([0x00]) # 0 = stop sampling
        self.__imu.close()
        
    def getData_Single(self, data):
        # Read whole buffer from device and return one sample per sensor
        dataRaw = self.__imu.read()
        t = time.time()
        bytesPerChannel = 2
        
        data.extend([t])
        for sensor in range(self.__numSensors):
            d = dataRaw[ (sensor*self.__numChannels*bytesPerChannel) : ((sensor+1)*self.__numChannels*bytesPerChannel) ]
            dataAll.extend(self.__getXyz(d, sensor, SensorType.Acc, self.__accelScale))
            dataAll.extend(self.__getXyz(d, sensor, SensorType.Gyro, self.__gyroScale))
            dataAll.extend(self.__getXyz(d, sensor, SensorType.Mag, self.__magScale))
            data.extend([self.__getTemp(d)])
        
    def getData_All(self, data):
        # Read whole buffer from device and return all available samples for all sensors
        dataRaw = self.__imu.read()
        t = time.time()
        bytesPerChannel = 2
        bytesPerSample = self.__numSensors*self.__numChannels*bytesPerChannel
        numSamples = len(dataRaw)/bytesPerSample
        
        for sample in range(numSamples):
            data.extend([t])
            for sensor in range(self.__numSensors):
                d = dataRaw[ ((sample*bytesPerSample)+(sensor*self.__numChannels*bytesPerChannel)) : ((sample*bytesPerSample)+((sensor+1)*self.__numChannels*bytesPerChannel)) ]
                data.extend(self.__getXyz(d, sensor, SensorType.Acc, self.__accelScale))
                data.extend(self.__getXyz(d, sensor, SensorType.Gyro, self.__gyroScale))
                data.extend(self.__getXyz(d, sensor, SensorType.Mag, self.__magScale))
                data.extend([self.__getTemp(d)])
    