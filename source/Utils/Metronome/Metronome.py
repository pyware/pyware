#!/usr/bin/python

from threading  import Thread, Event 
import time

class Metronome(Thread):
    def __init__(self):
        Thread.__init__(self)
        
        self.event      = Event()
        self._exitFlag  = False
        self.loopRate   = 1
        
    def setLoopRate(self, loopRate):
        # Set loop rate in seconds
        self.loopRate = loopRate

    def run(self):
        #print "Starting Metronome"
        while not self._exitFlag:
            time.sleep(self.loopRate)
            self.event.set()
            self.event.clear()
        #print "Exiting Metronome"
        
    def wait(self):
        self.event.wait()
        
    def join(self):
        self._exitFlag = True
        self.event.set()
        self.event.clear()
        super(Metronome, self).join()

    """
    # Test run with compensated sleep time    
    def run(self):
        #print "Starting Metronome"
        import datetime
        prevTime = datetime.datetime.now()
        while not self._exitFlag:
            delta = (datetime.datetime.now()-prevTime).total_seconds()
            time.sleep(self.loopRate-delta)
            self.event.set()
            self.event.clear()
            prevTime = datetime.datetime.now()
        #print "Exiting Metronome"
    """    