#!/usr/bin/python

from struct import unpack, pack, calcsize
import binascii

class Utils:
    def __init__(self):
        pass
        
    def scale(self, input, minIn, maxIn, minOut, maxOut):
        if input < minIn:
            input = minIn
        elif input > maxIn:
            input = maxIn
        return float(input-minIn)/float(maxIn-minIn)*float(maxOut-minOut)+minOut
        
    def packList(self, format, data):
        return bytes().join(pack(format, val) for val in data)
       
    def unpackList(self, format, bytes):
        formatSize = calcsize(format)
        byteStrSize = len(bytes)
        listSize = int(byteStrSize/formatSize)
        data = []
        for i in range(listSize):
            data.append(unpack(format, bytes[(i*formatSize):((i*formatSize)+formatSize)])[0])
        return data
        
    def crc32(self, string):
        return binascii.crc32(string) & 0xffffffff
        
    #def getClassName(self, obj):
    #    return obj.__name__.split(".")[-1]
    
    def mean(self, data):
        return float(sum(data)) / max(len(data), 1)
