

def readImu():
    data    = [0]*10
    accel   = []
    gyro    = []
    mag     = []
    temp    = []
    
    numDev  = 4
    
    for i in range(numDev):
        accel.append([0,1,2])
        gyro.append([3,4,5])
        mag.append([6,7,8])
        temp.append(100)
        
    print accel
    print gyro
    print mag
    print temp

    for i in range(3): # x,y,z
        a = []
        g = []
        m = []
        for j in range(numDev):
            #print str(i)+" "+str(j) + " " + str(accel[j][i])
            a.append(accel[j][i])
            g.append(gyro[j][i])
            m.append(mag[j][i])
        print a
        print g
        print m
        data[i]     = mean(a)
        data[i+3]   = mean(g)
        data[i+6]   = mean(m)
        print data

    data[9] = mean(temp)
    
    return data
        
def mean(data):
    return float(sum(data)) / max(len(data), 1)
    
print readImu()