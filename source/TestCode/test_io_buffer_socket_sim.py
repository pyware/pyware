#!/usr/bin/python

import sys, time
sys.path.append("../")

from IO.SimulatedDeviceReader.SimulatedDeviceReader         import SimulatedDeviceReader
from IO.SimulatedDeviceWriter.SimulatedDeviceWriter         import SimulatedDeviceWriter
from IO.SocketHandler.SocketHandler                         import SocketHandler
from Buffer.RingBuffer.RingBuffer                           import RingBuffer

numChIn     = 18
numChOut    = 18

bufferIn    = RingBuffer(10000, 1+numChIn) # BufferSize, ChunkSize (time + channels)
bufferOut   = RingBuffer(100, numChOut)

# Create reader/writer stream and socket handler objects. 
# All are threaded and share IO buffer.
dr = SimulatedDeviceReader(bufferIn, numChIn)
dw = SimulatedDeviceWriter(bufferOut, numChOut)
sh = SocketHandler(bufferIn, bufferOut)

dr.setLoopRate(0.05)
dw.setLoopRate(0.1)
sh.setLoopRate(0.1)

dr.start()
dw.start()
sh.start()

while True:
    try:
        time.sleep(1)
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
 
dr.join()
dw.join()
sh.join()
