#!/usr/bin/python

import time, datetime, math, sys, random
sys.path.append("../")

from Utils.Utils import Utils

print "Int"
chunkSize = 6
bufferSize = 200
print int(5.99999)
print int(6.00001)
print bufferSize%chunkSize
#bufferSize = bufferSize+(bufferSize%chunkSize)
#print bufferSize
print divmod(bufferSize, chunkSize)
print divmod(1000, 12)
print 1000%12

print "Time"
ts = time.time()
cs = time.clock()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
print ts
print st
print datetime.datetime.utcnow()
print datetime.datetime.now()
print time.clock()
print time.gmtime()
time.sleep(1)
print "Delta time = %f" %(time.time() - ts)
print time.clock() - cs

print "Math"
print 90%4
print math.floor(26/7)
print int(1.1)
print int(2.9999999)

print "Pack"
u = Utils()
floatList = [1.0, 2.0, 3.0]
print floatList
bytes = u.packList('>d', floatList)
print bytes
print len(bytes)
print u.unpackList('>d', bytes)

intList = [1, 2, 3]
print intList
bytes = u.packList('>I', intList)
print bytes
print len(bytes)
print u.unpackList('>I', bytes)

print "Split"
s = "hello.again.world"
print s.rfind(".")
sa = s.split(".")
print sa
print sa[-1]

print "Random"
print random.randrange(0,1000)

print "Hex"
print 0b11000000
h = 0xC0
print type(h)
print h
print int(h)
print str(hex(h))
print [30 | 128,0]

print "Array"
a = ["a", "b"]
b = ["c", "d"]
data1 = [0,1,2,3,4,5]
data2 = [0,1,2,3,4]
data1.extend(data2)
data1.insert(0,9)
print data1
print data1[:len(data1)/2]
print data1[len(data1)/2:]
print data2[:len(a)]
print data2[len(a):]
print data2[len(a):len(a)+len(b)]
print [1,2,3].insert(0,9)

print "Check"
if "":
    print "Empty"
else:
    print "Not empty"

print "Error"
raise Exception("Error1")
