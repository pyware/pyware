#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus.I2C.Devices.Adafruit.Adafruit_ADXL345_I2C import Adafruit_ADXL345_I2C
from Bus.SPI.Devices.Adafruit.Adafruit_ADXL345_SPI import Adafruit_ADXL345_SPI

# Create SPI refs acc sensor
i2c = Adafruit_ADXL345_I2C()
spi = Adafruit_ADXL345_SPI(0,0)
#sensor.setSpeed(400000)
#sensor.setMode(3)
 
while True:
    try:
        # Read acc sensors
        print "-------------"
        print "Bus [X, Y, Z]"
        print "I2C %s" %i2c.read()
        print "SPI %s" %spi.read()
        #sensor.outputText()

        time.sleep(1)
        
    except KeyboardInterrupt:
        print "\nStopped"
        break

sensor.close()