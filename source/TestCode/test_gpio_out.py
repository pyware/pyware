#!/usr/bin/python

import Adafruit_BBIO.GPIO as GPIO
import time

pins = ["P9_42"]
for i in pins:
    GPIO.setup(i, GPIO.OUT)

print "Setting %s" %pins[0]
for i in range(5):
    GPIO.output(pins[0], GPIO.LOW)
    print "LOW"
    time.sleep(1)
    GPIO.output(pins[0], GPIO.HIGH)
    print "HIGH"
    time.sleep(1)
    
print GPIO.LOW
print GPIO.HIGH

GPIO.cleanup()