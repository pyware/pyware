#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.AnalogDevices.AD_AD5421               import AD_AD5421
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(1)
spi.setSpeed(100000)

sensor = AD_AD5421(spi,3)
sensor.setup()

#sensor.reset()
#time.sleep(1)

sensor.setControl(wdTimeout=sensor.AD_AD5421_WD_5000, wdDisable=0, faultRbDisable=1, spiAlarmDisable=0, 
                   minCurrentEnable=0, adcInput=0, adcEnable=1, intRefDisable=0, vloopFaultEnable=1)

print "Get Control: %s" %sensor.getControl()
print "Get DAC: %s" %sensor.getDac()
print "Get Offset: %s" %sensor.getOffset()
print "Get Gain: %s" %sensor.getGain()
print "Get Fault reg: %s" %sensor.getFaultReg()

#sensor.setRange(0) # 4-20 mA
sensor.setRange(2) # 3.2-24.0 mA
print "Get Range: %s" %sensor.getRange()

#sensor.forceAlarmCurrent()
#time.sleep(4)

#sensor.setControl()
#sensor.getControl()

#print "DAC: %s" %sensor.getDac()

#sensor.setDac(0xFFFF)
#sensor.setCurrent(0)
#sensor.loadDac()
#time.sleep(4)

while True:
    try:
        print "===================="
        dac = 0x0000
        current = 3.2
        print "Set DAC = %d" %dac
        sensor.setDac(dac)
        #print "Set Current = %f" %current
        #sensor.setCurrent(current)
        #sensor.loadDac()
        time.sleep(0.1)
        print "Get DAC: %s" %sensor.getDac()
        print "Get Current: %s" %sensor.getCurrent()
        print "Get Fault reg: %s" %sensor.getFaultReg()
        time.sleep(4)
        
        print "--------------------"
        #dac = 0xF800
        dac = 0xFFFF
        current = 24.0
        print "Set DAC = %d" %dac
        sensor.setDac(dac)
        #print "Set Current = %f" %current
        #sensor.setCurrent(current)
        #sensor.loadDac()
        time.sleep(0.1)
        print "Get DAC: %s" %sensor.getDac()
        print "Get Current: %s" %sensor.getCurrent()
        print "Get Fault reg: %s" %sensor.getFaultReg()
        time.sleep(4)
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
   
"""
stop = False
count = 60000
while not stop:
    try:
        sensor.setDac(count)
        print "DAC: %s" %sensor.getDac()
        fault = sensor.getFaultReg()
        print "Fault reg: %s" %fault
        if not fault[0]:
            break
        count += 10
        time.sleep(0.1)
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
"""        
print "Get DAC: %s" %sensor.getDac()        
print "Get Fault reg: %s" %sensor.getFaultReg()
spi.close()