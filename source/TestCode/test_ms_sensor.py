#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.MeasurementSpecialties.MS_86BSD_015V.MS_86BSD_015V import MS_86BSD_015V

# Create SPI ref pressure sensor
sensor = MS_86BSD_015V(0,0)
sensor.setSpeed(400000)
 
while True:
    try:
        # Read pressure sensor
        sensor.read()
        sensor.outputText()

        time.sleep(1)
        
    except KeyboardInterrupt:
        print "\nStopped"
        break

sensor.close()