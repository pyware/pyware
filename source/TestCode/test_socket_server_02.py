import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('192.168.16.115', 10000)
print >>sys.stderr, 'Starting up on %s:%s' % server_address
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    # Wait for a connection
    print >>sys.stderr, 'Waiting for a connection.'
    connection, client_address = sock.accept()
    
    try:
        print >>sys.stderr, 'Connection from', client_address

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(16)
            print >>sys.stderr, 'Received data: "%s"' % data
            if data:
                print >>sys.stderr, 'Sending data back to the client.'
                connection.sendall(data)
            else:
                print >>sys.stderr, 'No more data from', client_address
                break
            
    finally:
        # Clean up the connection
        print >>sys.stderr, 'Close connection.'
        connection.close()