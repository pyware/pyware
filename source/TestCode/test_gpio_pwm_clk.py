#!/usr/bin/python

import Adafruit_BBIO.PWM as PWM
import time

# Pin order: EHRPWM1A, EHRPWM1B, EHRPWM2A, EHRPWM2B
#pins = ["P9_14", "P9_16", "P8_19", "P8_13"]

# Only (1A and 2A) or (1B and 2B) work together. Not all at the same time.
#pins = ["P9_14", "P8_19"] # 1A and 2A
#pins = ["P9_16", "P8_13"] # 1B and 2B
pins = ["P9_14"] # 1A
#pins = ["P8_19"] # 2A

def start():
    for pin in pins:
        #PWM.start(channel, duty, freq=2000, polarity=0)
        print "Starting pin %s" %pin
        PWM.start(pin, 50, 1000000, 0) # 1 MHz

def stop():
    for pin in pins:
        print "Stopping pin %s" %pin
        PWM.stop(pin)
    PWM.cleanup()

def set_duty(duty):
    for pin in pins:
        print "Setting duty %f on pin %s" %(duty, pin)
        PWM.set_duty_cycle(pin, duty)
    
def set_freq(freq):
    for pin in pins:
        print "Setting freq %f on pin %s" %(freq, pin)
        PWM.set_frequency(pin, freq)

try:        
    start()
    time.sleep(1)
    #set_duty(50)
    #set_freq(100000)
    
    while True:
        try:
            time.sleep(1)
            
        except KeyboardInterrupt:
            print "\nStopped by user."
            break
            
finally:
    stop()