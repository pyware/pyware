#!/usr/bin/python

import Adafruit_BBIO.GPIO as GPIO
import time

GPIO.setup("P8_14", GPIO.IN)

print "Reading P8_14"
if GPIO.input("P8_14"):
    print("HIGH")
else:
    print("LOW")
    
print GPIO.input("P8_14")

GPIO.setup("P8_14", GPIO.IN)
print "Wait for rising edge..."
GPIO.wait_for_edge("P8_14", GPIO.RISING)
print "Edge detected!"

GPIO.setup("P8_14", GPIO.IN)
print "Wait for falling edge event..."
GPIO.add_event_detect("P8_14", GPIO.FALLING)
while True: 
    if GPIO.event_detected("P8_14"):
        print "Event detected!"
        break
    time.sleep(1)
    
GPIO.cleanup()