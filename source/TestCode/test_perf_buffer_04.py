#!/usr/bin/python

import time
from collections import deque
import numpy as np

class RingBuffer():
    "A 1D ring buffer using numpy arrays"
    def __init__(self, length):
        self.data = np.zeros(length, dtype='f')
        self.index = 0

    def extend(self, x):
        "adds array x to ring buffer"
        x_index = (self.index + np.arange(x.size)) % self.data.size
        self.data[x_index] = x
        self.index = x_index[-1] + 1

    def get(self):
        "Returns the first-in-first-out data in the ring buffer"
        idx = (self.index + np.arange(self.data.size)) %self.data.size
        #idx = self.index % self.data.size
        return self.data[idx]

def ringbuff_numpy_test():
    ringlen = 100000
    ringbuff = RingBuffer(ringlen)
    for i in range(40):
        ringbuff.extend(np.ones(10000, dtype='f')) # write
        ringbuff.get() #read
        
def ringbuff_numpy_test_2():
    ringlen = 10
    ringbuff = RingBuffer(ringlen)
    for i in range(4):
        ringbuff.extend(np.arange(3, dtype='f')) # write
        print ringbuff.get() #read
       
def ringbuff_deque_test():
    ringlen = 100000
    ringbuff = deque(np.zeros(ringlen, dtype='f'), ringlen)
    for i in range(40):
        ringbuff.extend(np.ones(10000, dtype='f')) # write
        np.array(list(ringbuff)) #read
        #ringbuff.popleft() #read

time1 = time.time()
#ringbuff_numpy_test()
ringbuff_numpy_test_2()
#ringbuff_deque_test()
time2 = time.time()

delta = time2-time1
print "Time = %f s" %delta
#print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))