#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.Adafruit.Adafruit_ADXL345_SPI_BB  import Adafruit_ADXL345_SPI
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                   import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi       import BeagleboneSpiGpio
import Adafruit_BBIO.ADC                                as ADC
import Adafruit_BBIO.GPIO                               as GPIO

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(3)

# Create I2C accel sensor
#sensor = Adafruit_ADXL345_SPI(0,0)
sensor = Adafruit_ADXL345_SPI(spi,0)
sensor2 = Adafruit_ADXL345_SPI(spi,1)

sensor.setup()
sensor2.setup()

pinsAi = ["P9_39", "P9_40", "P9_37", "P9_38", "P9_33", "P9_36", "P9_35"]
pinsDi = ["P8_8", "P8_10", "P8_9", "P8_7"]

ADC.setup()
for pin in pinsDi:
    GPIO.setup(pin, GPIO.IN)

xyz1 = None
xyz2 = None
ai = None
di = None
    
num = 1000
time1 = time.time()

for i in range(num):
    data = []
    # SPI
    xyz1 = sensor.read()
    data.append(xyz1)
    xyz2 = sensor2.read()
    data.append(xyz2)
    # AI
    for pin in pinsAi:
        ai = ADC.read(pin)*1.8
        data.append(ai)
    # DI
    for pin in pinsDi:
        di = float(GPIO.input(pin))
        data.append(di)
"""
for i in range(num):
    data = []
    # SPI
    data.extend(sensor.read())
    data.extend(sensor2.read())
    # AI
    for pin in pinsAi:
        data.append(ADC.read(pin)*1.8)
    # DI
    for pin in pinsDi:
        data.append(float(GPIO.input(pin)))
"""
time2 = time.time()

delta = time2-time1
#print "SPI = %s, %s, AI = %s, DI = %s" %(xyz1, xyz2, ai, di)
print "Data = %s" %data
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))

GPIO.cleanup()
spi.close()