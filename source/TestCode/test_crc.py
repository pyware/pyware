#!/usr/bin/python

import hashlib, zlib, binascii, md5

#print hashlib.sha224("Nobody inspects the spammish repetition").hexdigest()

print "zlib"
print zlib.crc32("hello world")

print "crc32"
print binascii.crc32("hello world")
# Or, in two pieces:
crc = binascii.crc32("hello")
crc = binascii.crc32(" world", crc) & 0xffffffff
print crc
print len(str(crc))
print len('0x%10x' % crc)
print len('{:#012x}'.format(crc))
print len(str(crc))

print "md5"
m = md5.new()
m.update("Nobody inspects")
m.update(" the spammish repetition")
print m.digest()
b = md5.new("Nobody inspects the spammish repetition").digest()
print b
print len(b)