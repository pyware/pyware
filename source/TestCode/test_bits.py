from struct import unpack, pack

def splitData(data):
    data = data << (16 - 12)
    print "Data2: %s" %data
    bit_str = str('{:016b}'.format(data))
    print "BS: %s" %bit_str
    data0 = int(bit_str[8:16],2)
    data1 = int(bit_str[0:8],2)
    print "d0, d1: %s, %s" %(data0, data1)
    return data1, data0
    
def setDac(ch, data):
    if data < 0:
        data = data + (1 << 12)
        print "Data1: %s" %data
    splitData(data)
    
value = -2047
setDac(0,value)
print  pack('>I', value)