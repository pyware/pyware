#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.AnalogDevices.AD_AD5724R              import AD_AD5724R
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(1)
spi.setSpeed(1000000)

# Create I2C accel sensor
#sensor = Adafruit_ADXL345_SPI(0,0)
sensor = AD_AD5724R(spi,0)
sensor.setup()

#sensor.clearDac()
#time.sleep(1)

sensor.setRange(4,5) # Set range +-10.8 V on all channels 
#sensor.setDac(0,1024)

sensor.setRef(1) # 1 = internal ref
#time.sleep(1)
#sensor.loadDac()
print "Power ref: %s" %sensor.getPower()

sensor.setPower(4,1) # Set power on for all channels
time.sleep(1)
#sensor.loadDac()
print "Power all: %s" %sensor.getPower()

#sensor.setControl(4)
#sensor.getControl()

print "DAC A: %s" %sensor.getDac(0)
print "Range A: %s" %sensor.getRange(0)

#sensor.clearDac()
#sensor.setDac(4,-1024)
#sensor.loadDac()
print "Control: %s" %sensor.getControl()

print "DAC A: %s" %sensor.getDac(0)
print "DAC B: %s" %sensor.getDac(1)
print "DAC C: %s" %sensor.getDac(2)
print "DAC D: %s" %sensor.getDac(3)

volts = [0,5,10,0,-5,-10]
chans = [0, 1, 2, 3]
while True:
    try:
        for volt in volts:
            print "--------------------------------"
            print "Set %d V" %volt
            for ch in chans:
                sensor.setVoltage(ch,volt)
                #print "DAC A: %s" %sensor.getDac(0)
                #print "DAC B: %s" %sensor.getDac(1)
                #print "DAC C: %s" %sensor.getDac(2)
                #print "DAC D: %s" %sensor.getDac(3)
                time.sleep(1)

        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break

sensor.setVoltage(4,0)        
sensor.clearDac()
sensor.setPower(4,0) # Set power off for all channels
print "Power: %s" %sensor.getPower()

spi.close()