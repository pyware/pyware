#!/usr/bin/python

import sys, time
sys.path.append("../")

import Adafruit_BBIO.PWM as PWM
from Bus_Adafruit.SPI.Microchip.MC_MCP3903                  import MC_MCP3903
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Use PWM as 1 MHz clock if not using crystal
#pin = "P9_14"
#PWM.start(pin, 50, 1000000, 0)
#time.sleep(1)

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(3)
spi.setSpeed(100000)

sensor = MC_MCP3903(spi,5)
#sensor.setup(osr=sensor.MC_MCP3903_OSR_256, highRes=True)
sensor.setup(osr=sensor.MC_MCP3903_OSR_64, highRes=False)
#time.sleep(1)
numCh = sensor.MC_MCP3903_NUM_CH

#sensor.setRef(2.364)

#for ch in range(numCh):
#    sensor.setGainCh(ch, sensor.MC_MCP3903_GAIN_4)
#sensor.setGainAll(sensor.MC_MCP3903_GAIN_4)

print "Get Status: %s" %sensor.getStatusRaw()
print "Get Config: %s" %sensor.getConfigRaw()
print "Get Gain: %s" %sensor.getGainRaw()
print "Get Mod: %s" %sensor.getModRaw()
print "Get Phase: %s" %sensor.getPhaseRaw()

for ch in range(numCh):
    print "Gain and Boost %d: %s" %(ch, sensor.getGain(ch))

#time.sleep(1)
while True:
    try:
        print "===================="
        #print "ADC all: %s" %sensor.getAdcAll()
        #print "ADC %d: %s V" %(5, sensor.getVoltage(5))
        
        #Cont read
        #volts = sensor.getVoltageCont()
        
        for ch in range(numCh):
            print "ADC %d: %s V" %(ch, sensor.getVoltage(ch))
            #print "ADC %d: %s" %(ch, sensor.getAdc(ch))
            
            #print "ADC %d: %s V" %(ch, volts[ch])
            #time.sleep(0.1)
        time.sleep(1)
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
        
spi.close()

#PWM.stop(pin)
#PWM.cleanup()