#!/usr/bin/python

from collections import deque
import threading, time

class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
        
    @property
    def average(self):
        if len(self) != 0:
            return float(sum(self))/len(self)
        else:
            return None
          
    def popleftall(self):
        buffer = []
        if len(self) != 0:
            for i in range(len(self)):
                buffer.append(self.popleft())
            return buffer
        else:
            return None
            
class AppendThread (threading.Thread):
    def __init__(self, buffer):
        threading.Thread.__init__(self)
        self.__exitFlag = False
        self.buffer = buffer
        self.name = "AppendThread"
        self.count = 0
        
    def run(self):
        print "Starting " + self.name
        self.count = 0
        self.startTime = time.time()
        while not self.__exitFlag:
            self.buffer.append(float(self.count))
            self.count = self.count + 1
        self.stopTime = time.time()
        print "Exiting " + self.name
        
    def join(self):
        self.__exitFlag = True
        super(AppendThread, self).join()
        
    def getCount(self):
        return self.count
        
    def getTime(self):
        return [self.startTime, self.stopTime]
    
size = 100000
cb = CircularBuffer(size)

at = AppendThread(cb)
at.start()
time.sleep(10)
at.join()
count1 = at.getCount()
time1 = at.getTime()

at = AppendThread(cb)
at.start()
for i in range(100):
    data = cb.popleftall()
    time.sleep(0.1)

print len(data)
at.join()
time2 = at.getTime()
count2 = at.getCount()

print "Append rate w/o concurrent pop = %f (items/s)" %(count1/(time1[1]-time1[0]))
print "Append rate with concurrent pop = %f (items/s)" %(count2/(time2[1]-time2[0]))
