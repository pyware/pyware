#!/usr/bin/python

import sys, time, os
from subprocess import check_output
sys.path.append("../")

import Adafruit_BBIO.PWM as PWM
from Bus_Adafruit.SPI.Microchip.MC_MCP3903                  import MC_MCP3903
from Bus_Adafruit.SPI.Microchip.MC_MCP3903_Filtered         import MC_MCP3903_Median, MC_MCP3903_Median2
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

def performanceSetup():
    # Set the BBB CPU in performance mode
    os.system("cpufreq-set -g performance")

    # Get the pid of the spi process
    spipid = check_output(["pidof","spi1"])

    # Change the runtime properties of the spi process
    os.system("chrt -p -r 99 " + str(spipid))
    # Give the spi process a lower ionice
    os.system("ionice -c1 -p " + str(spipid))
    # Lower this process nice
    os.system("renice -n -20 -p " + str(spipid))

    # Get this (python script) process PID
    thispid = os.getpid()

    # Change the runtime properties of this process
    os.system("chrt -p -r 99 " + str(thispid))
    # Give the this process a lower ionice
    os.system("ionice -c1 -p " + str(thispid))
    # Lower this process nice
    os.system("renice -n -20 -p " + str(thispid))
    
# Use PWM as 1 MHz clock, if not using crystal
#pin = "P9_14"
#PWM.start(pin, 50, 1000000, 0)
#time.sleep(1)

performanceSetup()

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(3)
spi.setSpeed(4000000)

#sensor = MC_MCP3903(spi,0)
sensor = MC_MCP3903_Median2(spi,0)
sensor.setup(osr=sensor.MC_MCP3903_OSR_256, highRes=True)
numCh = sensor.MC_MCP3903_NUM_CH

#sensor.setGainAll(sensor.MC_MCP3903_GAIN_2)

print "Get Status: %s" %sensor.getStatusRaw()
print "Get Config: %s" %sensor.getConfigRaw()
print "Get Gain: %s" %sensor.getGainRaw()
print "Get Mod: %s" %sensor.getModRaw()
print "Get Phase: %s" %sensor.getPhaseRaw()

print "Perf test started..."
num = 10000
time1 = time.time()
for i in range(num):
    adc = []
    #for ch in range(numCh):
    #    adc.append(sensor.getVoltage(ch))
        #adc = sensor.getVoltage(ch)
    adc = sensor.getVoltageCont()
time2 = time.time()
print "ADC: %s" %adc

delta = time2-time1
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))

spi.close()

#PWM.stop(pin)
#PWM.cleanup()
