# https://pypi.python.org/pypi/paho-mqtt/1.1

import paho.mqtt.client as mqtt
import time

from struct import unpack, pack, calcsize
import binascii

class MqttHandler(object):
    def __init__(self, url, port): 
        
        self.client = mqtt.Client()
        self.client.on_connect      = self.__on_connect
        self.client.on_disconnect   = self.__on_disconnect
        self.client.on_message      = self.__on_message
        
        self.url = url
        self.port = port
    
    # The callback for when the client receives a CONNACK response from the server.
    def __on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        #client.subscribe("$SYS/#")
        client.subscribe("dvelab/#")
        
    # The callback for when the client sends a disconnect message.
    def __on_disconnect(self, client, userdata, rc):
        print("Disconnected with result code "+str(rc))

    # The callback for when a PUBLISH message is received from the server.
    def __on_message(self, client, userdata, msg):
        if msg.topic == "dvelab/test01":
            #data = self.decodeMsg(msg.payload)
            data = self.decodeMsgWithCrc32(msg.payload)
            print(msg.topic+" "+str(data))
            #print data
        else:
            print(msg.topic+" "+str(msg.payload))

    def encodeMsg(self, data):
        bs = bytes().join(pack('>d', val) for val in data)
        ba = bytearray(bs)
        ba_crc = bytearray(pack('>I', self.crc32(bs)) + bs)
        #print bs
        #print ba
        #print ba_crc
        return ba_crc
    
    def decodeMsg(self, bytes):
        formatSize = calcsize('>d')
        byteStrSize = len(bytes)
        listSize = int(byteStrSize/formatSize)
        data = []
        for i in range(listSize):
            data.append(unpack('>d', bytes[(i*formatSize):((i*formatSize)+formatSize)])[0])
        return data
        
    def decodeMsgWithCrc32(self, msg):
        crc_ok = False
        if msg:
            crc = msg[:4] # First 4 bytes are encoded crc
            payload = msg[4:] # Rest is encoded payload
            crc_local = self.crc32(payload)
            crc_remote = unpack('>I', crc)[0]
            crc_ok = (crc_local == crc_remote)
            if crc_ok:
                # Unpacks received byte string to double data list
                #print crc_local
                #print crc_remote
                return self.decodeMsg(payload)
            else:
                print crc_local
                print crc_remote
                raise Exception("CRC missmatch: Received CRC do not match calculated CRC.")
        else:
            return None
    
    def crc32(self, string):
        return binascii.crc32(string) & 0xffffffff
    
    def test(self):
        self.client.connect(self.url, self.port, 60)
        self.client.loop_start()
        time.sleep(1)

        for i in range(5):
            value = float(i+0.1)
            data = [value, value+1]
            self.client.publish("dvelab/test01", self.encodeMsg(data))
            self.client.publish("dvelab/test02", i*10)
            self.client.publish("dvelab/test03", i*100)
            time.sleep(1)

        self.client.loop_stop(force=False)
        self.client.disconnect()

    """
    def test2(self):
        self.client.connect(self.url, self.port, 60)
        self.client.loop_forever() # Blocking
        time.sleep(1)

        for i in range(5):
            self.client.publish("dvelab/test01", i))
            self.client.publish("dvelab/test02", i*10)
            self.client.publish("dvelab/test03", i*100)
            time.sleep(1)

        self.client.disconnect()
    """
    
mqtt = MqttHandler("iot.eclipse.org", 1883)
mqtt.test()
#mqtt.test2()