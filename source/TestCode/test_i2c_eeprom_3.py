from smbus import SMBus

smb=SMBus(1);
slaveaddr=0x54;

def eeprom_set_current_address(addr):
    a1=addr/256;
    a0=addr%256;
    smb.write_i2c_block_data(slaveaddr,a1,[a0]);

def eeprom_write_block(addr,data):
    a1=addr/256;
    a0=addr%256;
    data.insert(0,a0);
    smb.write_i2c_block_data(slaveaddr,a1,data);

def eeprom_read_byte(addr):
    eeprom_set_current_address(addr);
    return smb.read_byte(slaveaddr);

eeprom_set_current_address(0);
#eeprom_write_block(1024,[3,1,4,1,5])
print eeprom_read_byte(x);

"""
for x in range(0,10):
    print eeprom_read_byte(x);

for x in range(1024,1030):
    print eeprom_read_byte(x);
"""