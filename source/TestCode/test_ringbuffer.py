#!/usr/bin/python

from collections import deque
import time

class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(iterable=[], maxlen=size)
        
    @property
    def average(self):
        if len(self) != 0:
            return float(sum(self))/len(self)
        else:
            return None
            
    @property
    def median(self):
        lst = sorted(list(self))
        l = len(lst)
        
        #import math
        if l < 1:
            return None
        if l %2 == 1:
            return float(lst[((l+1)/2)-1])
        if l %2 == 0:
            return float(sum(lst[(l/2)-1:(l/2)+1]))/2.0
          
    def getall(self):
        buffer = []
        if len(self) != 0:
            for i in range(len(self)):
                buffer.append(self.popleft())
            return buffer
        else:
            return None
            
    def appendChunk(self, data):
        for i in data:
            self.append(i)

cb = CircularBuffer(10)
print cb

print "====== append i ======"
for i in range(20):
    cb.append(i)
    print "@%s, Average: %s, Median: %s" % (cb, cb.average, cb.median)

print "====== remove 15 ======"
cb.remove(15)
print "@%s, Average: %s" % (cb, cb.average)

print "====== count 11 ======"
print "return %d" %cb.count(11)
print "@%s, Average: %s" % (cb, cb.average)
 
print "====== popleft ======" 
for i in range(5):
    cb.popleft()
    print "@%s, Average: %s" % (cb, cb.average)

print "====== len ======"
print len(cb)

print "====== append array ========"
cb.appendChunk([101, 102, 103])
print cb

print "====== getall ======"
print cb.getall()
print "@%s, Average: %s" % (cb, cb.average)

print "====== extend array ========"
cb.extend([101, 102, 103, 99, 98, 97, 5, 2000])
print cb
print "Sorted: %s" %sorted(list(cb))
    
print "Average: %s" % (cb.average)    
print "Median: %s" %cb.median
    
print "====== clear ======"
cb.clear()
print "@%s, Average: %s" % (cb, cb.average)

print "======= performance ========"
size = 10000
cb = CircularBuffer(size)
t1 = time.time()
for i in range(size):
    cb.append(i)
t2 = time.time()
for i in range(size):
    cb.append(i)
t3 = time.time()
#for i in range(size):
#    cb.popleft()
cb.getall()
t4 = time.time()
delta1 = t2 - t1
delta2 = t3 - t2
delta3 = t4 - t3
deltaTot = t4 - t1
print "Delta1 = %s" %delta1
print "Delta2 = %s" %delta2
print "Delta3 = %s" %delta3
print "DeltaTot = %s" %deltaTot
print "Rate = %f" %(deltaTot/size)
    