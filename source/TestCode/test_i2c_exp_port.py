#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.I2C.TexasInstruments.TI_TCA6416A import TI_TCA6416A_SP

exp0 = TI_TCA6416A_SP(port=0)
exp1 = TI_TCA6416A_SP(port=1)

exp0.setup()
exp1.setup()

#print exp0.getNumCh()
#print exp1.getNumCh()

exp0.setConfig(0x00) # Set all ports as outputs
exp1.setConfig(0xFF) # Set all ports as inputs

while True:
    try:
        #print '[Inputs: {:08b}]'.format(exp1.read())
        print "-------------------------"
        print '[Outputs: 00000000]'
        exp0.writeOutputs(0x00)
        #exp0.writeList([0,1,0,1,0,1,0,1])
        print '[Inputs: {:08b}]'.format(exp1.readInputs())
        #print exp1.readList()
        time.sleep(1)
        print "-------------------------"
        print '[Outputs: 11111111]'
        exp0.writeOutputs(0xFF)
        #exp0.writeList([1,0,1,0,1,0,1,0])
        print '[Inputs: {:08b}]'.format(exp1.readInputs())
        #print exp1.readList()
        time.sleep(2)
    
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
    
exp0.writeOutputs(0x00)

