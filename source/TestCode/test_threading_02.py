#!/usr/bin/python

import threading
import time

class myThread (threading.Thread):
    def __init__(self, id, name=""):
        threading.Thread.__init__(self)
        self.__exitFlag = False
        self.id = id
        if len(name):
            self.name = name
        else:
            self.name = "Thread_%d" %self.id
        
    def run(self):
        print "Starting " + self.name
        while not self.__exitFlag:
            self.work()
        print "Exiting " + self.name
        
    def work(self):
        print "%s: Doing some work..." %self.name
        time.sleep(1)
        print "%s: Work done!" %self.name
        
    def start(self):
        print "Started"
        super(myThread, self).start()
    
    def join(self):
        self.__exitFlag = True
        super(myThread, self).join()

# Create new threads
thread1 = myThread(1, "test1")
thread2 = myThread(2)
thread1.start()
thread2.start()

time.sleep(5)
   
# Wait for all threads to complete
thread1.join()
print "Thread1 done"
thread2.join()
print "Thread2 done"
print "Exiting Main Thread"