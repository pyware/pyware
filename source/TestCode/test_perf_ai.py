#!/usr/bin/python

import sys, time
sys.path.append("../")

import Adafruit_BBIO.ADC as ADC

pinsAi = ["P9_39", "P9_40", "P9_37", "P9_38", "P9_33", "P9_36", "P9_35"]
#pinsAi = ["P9_40"]

ADC.setup()
ai = None
    
print "Perf test started..."    
num = 10000
time1 = time.time()

for i in range(num):
    # AI
    data = []
    for pin in pinsAi:
        ai = ADC.read(pin)*1.8
        data.append(ai)

time2 = time.time()

delta = time2-time1
#print "AI = %s" %ai
print "Data = %s" %data
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))