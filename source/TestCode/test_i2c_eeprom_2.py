#!/usr/bin/python

import sys, time
sys.path.append("../")

#from Bus_Adafruit.I2C.Catalyst.Catalyst_CAT24C256 import Catalyst_CAT24C256
from Bus_Adafruit.I2C.Catalyst.Catalyst_CAT24C256_2 import Catalyst_CAT24C256

eeprom = Catalyst_CAT24C256(0x50, 1)
    
while True:
    try:
        print '{:08b}'.format(eeprom.read(2))
        time.sleep(1)
    except KeyboardInterrupt:
        print "\nStopped by user."
        break

eeprom.close