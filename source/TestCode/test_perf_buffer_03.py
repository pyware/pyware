#!/usr/bin/python

from collections import deque
import threading, time, sys
import numpy as np

sys.path.append("../")

#from Buffer.RingBuffer.RingBuffer  import RingBuffer

class RingBuffer(object):
    def __init__(self, bufferSize, chunkSize=1):
        # Set buffer size to an even number of chunk sizes
        div, rem = divmod(bufferSize, chunkSize)
        if rem > 0:
            bs = chunkSize*(div+1)
        else:
            bs = bufferSize
        self.dq = deque(maxlen=bs)
        #super(RingBuffer, self).__init__(maxlen=bs)
        
        self.__chunkSize = chunkSize
        self.__busy = False
        #print "Actual buffer size %d" %bs
           
    @property
    def average(self):
        if len(self.dq) != 0:
            return float(sum(self))/len(self.dq)
        else:
            return None
            
    def __popSize(self, size):
        buffer = []
        if len(self.dq) >= size:
            #self.busy()
            for i in range(size):
                buffer.append(self.dq.popleft())
            #self.done()
            return buffer
        else:
            return None
                
    def popAll(self):
        return self.__popSize(len(self.dq))
                 
    def popChunk(self):
        return self.__popSize(self.__chunkSize)
      
    def popChunkAll(self):
        # Get the remaining number of whole chunks in buffer
        chunks = int(len(self.dq)/self.__chunkSize)
        #print "Chunks to pop %d" %chunks
        return self.__popSize(chunks*self.__chunkSize)
        
    def appendChunk(self, data):
        #self.busy()
        for i in data:
            self.dq.append(i)
        #self.done()
        
class RingBuffer2(object):
    def __init__(self, bufferSize, chunkSize=1):
        #self.data = np.zeros(chunkSize, dtype='f')
        self.size = int(bufferSize/chunkSize)
        self.dq = deque([], self.size)
        #super(RingBuffer, self).__init__([0.0]*chunkSize, self.size)
    
    def __popSize(self, size):
        buffer = []
        if len(self.dq) >= size:
            for i in range(size):
                buffer.extend(self.dq.popleft())
            return buffer
        else:
            return None
                
    def popAll(self):
        return self.__popSize(len(self.dq))
    
    def popChunk(self):
        return self.__popSize(1)
      
    def popChunkAll(self):
        # Get the remaining number of whole chunks in buffer
        chunks = int(len(self.dq))
        #print "Chunks to pop %d" %chunks
        return self.__popSize(chunks)
        
    def appendChunk(self, data):
        self.dq.append(data)
        
    def getDq(self):
        return self.dq

class AppendThread (threading.Thread):
    def __init__(self, buffer, chunkSize):
        threading.Thread.__init__(self)
        self.__exitFlag = False
        self.buffer = buffer
        self.name = "AppendThread"
        self.count = 0
        
        # Create dummy data
        self.chunk = []
        for i in range(chunkSize):
            self.chunk.append(float(i))
            
        self.d1 = [1.0, 2.0, 3.0]
        self.d2 = [4.0, 5.0, 6.0]
        self.d3 = [7.0, 8.0, 9.0]
    
    def readData(self):
        data = []
        t = time.time()
        #data.append(t)
        data.extend(self.d1)
        data.extend(self.d2)
        data.extend(self.d3)
        data.insert(0, t)
        return data
        
    def run(self):
        print "Starting " + self.name
        self.count = 0
        self.startTime = time.time()
        #data = self.readData()  
        while not self.__exitFlag:
            data = self.readData()
            self.buffer.appendChunk(data)
            self.count = self.count + 1
        self.stopTime = time.time()
        print "Exiting " + self.name
        
    def join(self):
        self.__exitFlag = True
        super(AppendThread, self).join()
        
    def getCount(self):
        return self.count
        
    def getTime(self):
        return [self.startTime, self.stopTime]
    
size = 100000
chunkSize = 10
#cb = RingBuffer(size, chunkSize)
cb = RingBuffer2(size, chunkSize)
print "------------------------"

"""
# Only for RingBuffer2
cb.appendChunk([1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.1])
cb.appendChunk([1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.1])
dq = cb.getDq()
print len(dq)
print dq
"""

at = AppendThread(cb, chunkSize)
at.start()
time.sleep(10)
at.join()

time1 = at.getTime()
count1 = at.getCount()
print "Append rate w/o concurrent pop = %f (chunks/s)" %(count1/(time1[1]-time1[0]))

at = AppendThread(cb, chunkSize)
at.start()
for i in range(100):
    #data = cb.popChunk()
    data = cb.popChunkAll()
    time.sleep(0.1)
print len(data)
at.join()

time2 = at.getTime()
count2 = at.getCount()
print "Append rate with concurrent pop = %f (chunks/s)" %(count2/(time2[1]-time2[0]))
