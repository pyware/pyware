#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus.SPI.SpiEngine.GpioSpiEngine.GpioSpiEngine  import GpioSpiEngine
from Bus.SPI.Devices.InvenSense.IS_MPU9250          import IS_MPU9250

class BbbGpioPins:
    CS0 = "P8_11"
    CS1 = "P8_12"
    CS2 = "P9_12"
    CS3 = "P9_15"
    CS_PINS = [CS0, CS1, CS2, CS3]

# Create SPI engine using GPIO as CS
spi = GpioSpiEngine(BbbGpioPins.CS_PINS)
spi.setMode(3)
spi.setSpeed(1000000)

# Create IMU device using the SPI engine
dev = IS_MPU9250(spi,0)
"""
dev0 = IS_MPU9250(spi,0)
dev1 = IS_MPU9250(spi,1)
dev2 = IS_MPU9250(spi,2)
dev3 = IS_MPU9250(spi,3)

for dev in [dev0, dev1, dev2, dev3]:

    print dev.getTemp()
    print dev.getAccel()
    #print dev.test()
"""

dev.setup()
dev.testRead()

while True:
    try:
        print "===================="
        print "Accel:   %s" %dev.getAccel()
        print "Gyro:    %s" %dev.getGyro()
        print "Mag:     %s" %dev.getMag()
        print "Temp:    %s" %dev.getTemp()
        #print dev.testRead()
        time.sleep(1)

    except KeyboardInterrupt:
        print "\nStopped by user."
        break

dev.cleanUp()
dev.testRead()

spi.close()
