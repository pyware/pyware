#!/usr/bin/python

import Adafruit_BBIO.ADC as ADC
import time
 
ADC.setup()

print "Reading P9_40"
value = ADC.read("P9_40") # Between 0-1
#value = ADC.read("AIN1")
voltage = value*1.8 # Convert to 1.8 V
raw = ADC.read_raw("P9_40")

print "Value = %f" %value
print "Voltage = %f" %voltage
print "Raw = %f" %raw
