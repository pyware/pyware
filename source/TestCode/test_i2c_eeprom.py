#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus.I2C.Devices.Catalyst.Catalyst_CAT24C256 import Catalyst_CAT24C256

eeprom = Catalyst_CAT24C256(4, 1, True)

#eeprom.writeToAddr([0x00,0x00], [0x11,0x22])
#eeprom.writeList(0x00, [[0x00, 0x11,0x22]])
#print eeprom.readFromAddr(0x00, 3)
#print eeprom.readList(0x00, 3)
#print eeprom.readFromAddr2([0x00,0x00], 3)
print eeprom.read()

"""
eeprom.write(0)
eeprom.write(0)
eeprom.write(0)

eeprom.write(0)
eeprom.write(0)
print eeprom.read()
"""

"""   
while True:
    try:
        print '{:08b}'.format(eeprom.read())
        time.sleep(1)
    except KeyboardInterrupt:
        print "\nStopped by user."
        break
"""