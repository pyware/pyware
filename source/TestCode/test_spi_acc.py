#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.Adafruit.Adafruit_ADXL345_SPI_BB      import Adafruit_ADXL345_SPI
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(3)
spi.setSpeed(100000)

# Create I2C accel sensor
#sensor = Adafruit_ADXL345_SPI(0,0)
#sensor1 = Adafruit_ADXL345_SPI(spi,0)
sensor2 = Adafruit_ADXL345_SPI(spi,1)

#sensor1.setup()
sensor2.setup()

while True:
    try:
        print "===================="
        #print "ACC1: %s" %sensor1.read()
        print "ACC2: %s" %sensor2.read()
        time.sleep(1)
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break

spi.close()
