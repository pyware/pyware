#!/usr/bin/python

import sys, time
sys.path.append("../")

import Adafruit_BBIO.GPIO as GPIO

pinsDi = ["P8_8", "P8_10", "P8_9", "P8_7"]

for pin in pinsDi:
    GPIO.setup(pin, GPIO.IN)

di = None
    
num = 10000
time1 = time.time()

for i in range(num):
    # DI
    data = []
    for pin in pinsDi:
        di = float(GPIO.input(pin))
        data.append(di)

time2 = time.time()

delta = time2-time1
print "DI = %s" %di
print "Data = %s" %data
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))

GPIO.cleanup()