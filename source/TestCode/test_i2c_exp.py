#!/usr/bin/python

import sys
sys.path.append("../")

from Bus_Adafruit.I2C.TexasInstruments.TI_TCA6416A import TI_TCA6416A
from time import sleep

exp = TI_TCA6416A()
exp.setup()

exp.setConfig(0x00, 0x00) # Set all ports as outputs
sleep(1)

exp.writeOutputs(0x00, 0x00)
sleep(1)
exp.writeOutputs(0xFF, 0xFF)
sleep(1)
exp.writeOutputs(0x00, 0x00)
sleep(1)
exp.write(0b1111111100000000)
sleep(1)
exp.write(0b0000000011111111)
sleep(1)
exp.write(0b1111111111111111)
sleep(1)
exp.write(0b0000000000000000)
sleep(1)

exp.setConfig(0xFF, 0xFF) # Set all ports as inputs

print '[Inputs]'
while True:
    print '{:016b}'.format(exp.read())
    sleep(1)