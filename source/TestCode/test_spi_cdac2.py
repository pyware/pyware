#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.AnalogDevices.AD_AD5420               import AD_AD5420
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(0)
spi.setSpeed(100000)

sensor = AD_AD5420(spi,4)
sensor.setup()

#sensor.reset()
#time.sleep(1)

print "Get Control: %s" %sensor.getControl()
print "Get DAC: %s" %sensor.getDac()
print "Get Status: %s" %sensor.getStatus()

sensor.setRange(2) # 0-24.0 mA
print "Get Range: %s" %sensor.getRange()

print "Get Control: %s" %sensor.getControl()
print "Get DAC: %s" %sensor.getDac()
print "Get Status: %s" %sensor.getStatus()

while True:
    try:
        print "===================="
        dac = 0x0000
        current = 0.0
        #print "Set DAC = %d" %dac
        #sensor.setDac(dac)
        print "Set Current = %f" %current
        sensor.setCurrent(current)
        time.sleep(0.1)
        print "Get DAC: %s" %sensor.getDac()
        print "Get Current: %s" %sensor.getCurrent()
        print "Get Status: %s" %sensor.getStatus()
        time.sleep(4)
        
        print "--------------------"
        #dac = 0xF800
        dac = 0xFFFF
        current = 24.0
        #print "Set DAC = %d" %dac
        #sensor.setDac(dac)
        print "Set Current = %f" %current
        sensor.setCurrent(current)
        time.sleep(0.1)
        print "Get DAC: %s" %sensor.getDac()
        print "Get Current: %s" %sensor.getCurrent()
        print "Get Status: %s" %sensor.getStatus()
        time.sleep(4)

    except KeyboardInterrupt:
        print "\nStopped by user."
        break

sensor.setCurrent(0)        
print "Get DAC: %s" %sensor.getDac()        
print "Get Status: %s" %sensor.getStatus()
spi.close()