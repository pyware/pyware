#!/usr/bin/python

import sys, time
import beaglebone_pru_adc as adc

capture = adc.Capture()

ema_pow = 16 # Set EMA (Exp moving average) smothening factor, 2^ema_pow
print "EMA = %d" %pow(2,ema_pow)
capture.ema_pow = ema_pow

capture.start()

num = 10000
time1 = time.time()

for i in range(num):
    # AI
    data = []
    timer = capture.timer
    values = capture.values
    for v in values:
        ema = float(v)/pow(2,ema_pow)
        ai = ema/4095*1.8
        data.append(ai)

time2 = time.time()

delta = time2-time1
#print "AI = %s" %ai
print "Data = %s" %data
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))

capture.stop() # ask driver to stop
capture.wait() # wait for graceful exit
capture.close()
