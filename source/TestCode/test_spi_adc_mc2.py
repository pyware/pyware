#!/usr/bin/python

import sys, time, datetime
sys.path.append("../")

from Bus_Adafruit.SPI.Microchip.MC_MCP3903                  import MC_MCP3903
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio


# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(0)
spi.setSpeed(1000000)

sensor = MC_MCP3903(spi,5)
sensor.setup(osr=sensor.MC_MCP3903_OSR_256, highRes=True)
#sensor.setup(osr=sensor.MC_MCP3903_OSR_64, highRes=True)
#time.sleep(1)
numCh = sensor.MC_MCP3903_NUM_CH

sensor.setPhaseRaw(0x000000)

#sensor.setRef(2.364)

#for ch in range(numCh):
#    sensor.setGainCh(ch, sensor.MC_MCP3903_GAIN_4)
sensor.setGainAll(sensor.MC_MCP3903_GAIN_1, boost=0)

print "Get Status: %s" %sensor.getStatusRaw()
print "Get Config: %s" %sensor.getConfigRaw()
print "Get Gain: %s" %sensor.getGainRaw()
print "Get Mod: %s" %sensor.getModRaw()
print "Get Phase: %s" %sensor.getPhaseRaw()

for ch in range(numCh):
    print "Gain and Boost %d: %s" %(ch, sensor.getGain(ch))

volt, data = sensor.getVoltageDebug(0)
print "Value = %f V, Data = %d" %(volt, data)

print "===================="
# Glitch debugging
gCount = 0
gLimitTol = 0.01
gLimitH = volt + gLimitTol
gLimitL = volt - gLimitTol
gMax = gLimitH
gMin = gLimitL
data = 0

print "Glitch counter started. Limit = %f to %f V." %(gLimitL, gLimitH)
time.sleep(1)


while True:
    try:
        #print "===================="
        #print "ADC all: %s" %sensor.getAdcAll()
        #print "ADC %d: %s V" %(5, sensor.getVoltage(5))
        
        #for ch in range(numCh):
        #for ch in range(1):
        #    volt, data = sensor.getVoltageDebug(ch)
        i = 0    
        volts, data = sensor.getVoltageCont()
        for volt in volts:
            if volt < gLimitL or volt > gLimitH:
                timeStr = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S.%f')
                gCount += 1
                if volt > gMax:
                    gMax = volt
                if volt < gMin:
                    gMin = volt
                print "%s   Glitch detected! Channel = %d, Value = %f V, Data = %d, Count = %d" %(timeStr, i, volt, data[i], gCount)
                print volts
                print data
                raise Exception("Glitch detected!") # Exception to stop loop
                
            i += 1    
            #print "ADC %d: %s V" %(ch, volt)
            
            #print "ADC %d: %s V" %(ch, sensor.getVoltage(ch))
            #print "ADC %d: %s" %(ch, sensor.getAdc(ch))
            #time.sleep(0.1)
        time.sleep(0.01)
        
    except KeyboardInterrupt:
        print "\nStopped by user."
        break

#volt, data = sensor.getVoltage2(0)
#print "Value = %f V, Data = %d" %(volt, data)
print volts
print data
print "Max = %f V, Min = %f V" %(gMax, gMin) 
spi.close()