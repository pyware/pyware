#!/usr/bin/python

from collections import deque
import time

class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
        
    @property
    def average(self):
        if len(self) != 0:
            return float(sum(self))/len(self)
        else:
            return None
          
    def getall(self):
        buffer = []
        if len(self) != 0:
            for i in range(len(self)):
                buffer.append(self.popleft())
            return buffer
        else:
            return None
            
size = 100000
cb = CircularBuffer(size)
t1 = time.time()
for i in range(size):
    cb.append(i)
t2 = time.time()
for i in range(size):
    cb.append(i)
t3 = time.time()
#for i in range(size):
#    cb.popleft()
cb.getall()
t4 = time.time()
delta1 = t2 - t1
delta2 = t3 - t2
delta3 = t4 - t3
deltaTot = t4 - t1
print "Time to append %d to empty = %s" %(size, delta1)
print "Time to append %d to full = %s" %(size, delta2)
print "Time to pop %d from full = %s" %(size, delta3)
print "Time total = %s" %deltaTot
#print "Rate = %f" %(deltaTot/size)