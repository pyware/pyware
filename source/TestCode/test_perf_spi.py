#!/usr/bin/python

import sys, time
sys.path.append("../")

from Bus_Adafruit.SPI.Adafruit.Adafruit_ADXL345_SPI_BB      import Adafruit_ADXL345_SPI
from Bus_Adafruit.SPI.SpiGpio.SpiGpio                       import SpiGpio
from Bus_Adafruit.SPI.BeagleboneSpi.BeagleboneSpi           import BeagleboneSpiGpio

# Create SPI engine
spi = SpiGpio(BeagleboneSpiGpio.CS_PINS)
spi.setMode(3)

# Create I2C accel sensor
#sensor = Adafruit_ADXL345_SPI(0,0)
sensor = Adafruit_ADXL345_SPI(spi,0)
sensor2 = Adafruit_ADXL345_SPI(spi,1)

num = 10000
time1 = time.time()
for i in range(num):
    xyz1 = sensor.read()
    xyz2 = sensor2.read()
time2 = time.time()
print "Sensor1: %s" %xyz1
print "Sensor2: %s" %xyz2

delta = time2-time1
print "Time to read %d values: %f s. Sample rate = %f S/s." %(num, delta, (num/delta))

spi.close()