#!/usr/bin/python

import threading, time

e = threading.Event()
threads = []
flag = True

def runner():
    tname = threading.current_thread().name
    while flag:
        print 'Thread waiting for event: %s' % tname
        e.wait()
        print 'Thread got event: %s' % tname
        time.sleep(0.1)

for t in range(10):
    t = threading.Thread(target=runner)
    threads.append(t)
    t.start()

while True:
    try:
        time.sleep(0.2)
        raw_input('Press enter to set and clear the event:')
        e.set()
        e.clear()
    except KeyboardInterrupt:
        print "Abort"
        break

time.sleep(0.1)        
flag = False
e.set()
e.clear()

for t in threads:
    t.join()
print 'All done.'