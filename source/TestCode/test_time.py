#!/usr/bin/python

import time, datetime, math, sys, ctypes
sys.path.append("../")

from Utils.Utils import Utils

print "Time"
ts = time.time()
c = time.clock()
dt = datetime.datetime.now()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S.%f')
print ts
print c
print dt
print st
print datetime.datetime.utcnow()
print datetime.datetime.now()
print datetime.datetime.now().microsecond
print time.gmtime()
delta1 = time.clock() - c
delta2 = datetime.datetime.now() - dt
print delta1
print delta2
print delta2.total_seconds()

print "================"
"""
def check_sleep(amount):
    start = datetime.datetime.now()
    time.sleep(amount)
    end = datetime.datetime.now()
    delta = end-start
    return delta.seconds + delta.microseconds/1000000.

error = sum(abs(check_sleep(0.050)-0.050) for i in xrange(100))*10
print "Average error is %0.2fms" % error
"""
